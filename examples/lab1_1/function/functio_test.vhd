


LIBRARY ieee;
USE ieee.std_logic_1164.ALL;


ENTITY functio_test IS
END functio_test;

ARCHITECTURE behavior OF functio_test IS 



  
  COMPONENT fucntion
    PORT(
      a : IN  std_logic;
      b : IN  std_logic;
      c : IN  std_logic;
      d : IN  std_logic;
      f : OUT  std_logic
      );
  END COMPONENT;
  

  signal a : std_logic := '0';
  signal b : std_logic := '0';
  signal c : std_logic := '0';
  signal d : std_logic := '0';

  signal f : std_logic;
  
BEGIN
  


  front1: process is
  begin
    
    a <= '0';
    wait for 80 NS;
    a <= '1';
    wait for 80 NS;
    a <= '0';
    
  end process front1;



  front2: process is
  begin
    
    b <= '0';
    wait for 40 NS;
    b <= '1';
    wait for 40 NS;
    b <= '0';

  end process front2;


  
  front3: process is
  begin
    
    c <= '0';
    wait for 20 NS;
    c <= '1';
    wait for 20 NS;
    c <= '0';
    
  end process front3;


  
  front4: process is
  begin
    
    d <= '0';
    wait for 10 NS;
    d <= '1';
    wait for 10 NS;  
    d <= '0';
    
  end process front4;



  uut: fucntion PORT MAP (
    a => a,
    b => b,
    c => c,
    d => d,
    f => f
    );


end;
