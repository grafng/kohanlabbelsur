<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="b" />
        <signal name="d" />
        <signal name="XLXN_14" />
        <signal name="XLXN_5" />
        <signal name="XLXN_15" />
        <signal name="XLXN_7" />
        <signal name="XLXN_16" />
        <signal name="a" />
        <signal name="c" />
        <signal name="XLXN_17" />
        <signal name="XLXN_19" />
        <signal name="f_out" />
        <signal name="XLXN_22" />
        <port polarity="Input" name="b" />
        <port polarity="Input" name="d" />
        <port polarity="Output" name="XLXN_14" />
        <port polarity="Output" name="XLXN_15" />
        <port polarity="Output" name="XLXN_16" />
        <port polarity="Input" name="a" />
        <port polarity="Input" name="c" />
        <port polarity="Output" name="XLXN_19" />
        <port polarity="Output" name="f_out" />
        <blockdef name="and3b3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="40" y1="-64" y2="-64" x1="0" />
            <circle r="12" cx="52" cy="-64" />
            <line x2="40" y1="-128" y2="-128" x1="0" />
            <circle r="12" cx="52" cy="-128" />
            <line x2="40" y1="-192" y2="-192" x1="0" />
            <circle r="12" cx="52" cy="-192" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="and3b1">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="40" y1="-64" y2="-64" x1="0" />
            <circle r="12" cx="52" cy="-64" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <block symbolname="and3b3" name="XLXI_9">
            <blockpin signalname="c" name="I0" />
            <blockpin signalname="b" name="I1" />
            <blockpin signalname="a" name="I2" />
            <blockpin signalname="XLXN_19" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_10">
            <blockpin signalname="d" name="I0" />
            <blockpin signalname="b" name="I1" />
            <blockpin signalname="a" name="I2" />
            <blockpin signalname="XLXN_14" name="O" />
        </block>
        <block symbolname="and3b1" name="XLXI_12">
            <blockpin signalname="a" name="I0" />
            <blockpin signalname="d" name="I1" />
            <blockpin signalname="c" name="I2" />
            <blockpin signalname="XLXN_15" name="O" />
        </block>
        <block symbolname="and3b1" name="XLXI_15">
            <blockpin signalname="d" name="I0" />
            <blockpin signalname="c" name="I1" />
            <blockpin signalname="b" name="I2" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_18">
            <blockpin signalname="XLXN_16" name="I0" />
            <blockpin signalname="XLXN_15" name="I1" />
            <blockpin signalname="XLXN_14" name="I2" />
            <blockpin signalname="XLXN_19" name="I3" />
            <blockpin signalname="f_out" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1920" y="1440" name="XLXI_10" orien="R0" />
        <instance x="1920" y="1728" name="XLXI_12" orien="R0" />
        <instance x="1920" y="2032" name="XLXI_15" orien="R0" />
        <branch name="b">
            <wire x2="1520" y1="848" y2="1008" x1="1520" />
            <wire x2="1520" y1="1008" y2="1312" x1="1520" />
            <wire x2="1920" y1="1312" y2="1312" x1="1520" />
            <wire x2="1520" y1="1312" y2="1840" x1="1520" />
            <wire x2="1520" y1="1840" y2="2080" x1="1520" />
            <wire x2="1920" y1="1840" y2="1840" x1="1520" />
            <wire x2="1728" y1="1008" y2="1008" x1="1520" />
            <wire x2="1728" y1="1008" y2="1040" x1="1728" />
            <wire x2="1936" y1="1040" y2="1040" x1="1728" />
        </branch>
        <branch name="d">
            <wire x2="1584" y1="848" y2="1376" x1="1584" />
            <wire x2="1920" y1="1376" y2="1376" x1="1584" />
            <wire x2="1584" y1="1376" y2="1600" x1="1584" />
            <wire x2="1920" y1="1600" y2="1600" x1="1584" />
            <wire x2="1584" y1="1600" y2="1968" x1="1584" />
            <wire x2="1584" y1="1968" y2="2080" x1="1584" />
            <wire x2="1920" y1="1968" y2="1968" x1="1584" />
        </branch>
        <branch name="XLXN_14">
            <wire x2="2272" y1="1312" y2="1312" x1="2176" />
            <wire x2="2272" y1="1312" y2="1424" x1="2272" />
            <wire x2="2512" y1="1424" y2="1424" x1="2272" />
            <wire x2="2608" y1="1424" y2="1424" x1="2512" />
            <wire x2="2272" y1="1424" y2="1424" x1="2192" />
        </branch>
        <branch name="XLXN_15">
            <wire x2="2272" y1="1600" y2="1600" x1="2176" />
            <wire x2="2272" y1="1488" y2="1488" x1="2192" />
            <wire x2="2512" y1="1488" y2="1488" x1="2272" />
            <wire x2="2608" y1="1488" y2="1488" x1="2512" />
            <wire x2="2272" y1="1488" y2="1600" x1="2272" />
        </branch>
        <branch name="XLXN_16">
            <wire x2="2320" y1="1904" y2="1904" x1="2176" />
            <wire x2="2320" y1="1552" y2="1904" x1="2320" />
            <wire x2="2464" y1="1552" y2="1552" x1="2320" />
            <wire x2="2512" y1="1552" y2="1552" x1="2464" />
            <wire x2="2608" y1="1552" y2="1552" x1="2512" />
            <wire x2="2464" y1="1552" y2="1632" x1="2464" />
        </branch>
        <branch name="a">
            <wire x2="1488" y1="656" y2="944" x1="1488" />
            <wire x2="1488" y1="944" y2="1248" x1="1488" />
            <wire x2="1920" y1="1248" y2="1248" x1="1488" />
            <wire x2="1488" y1="1248" y2="1664" x1="1488" />
            <wire x2="1488" y1="1664" y2="2080" x1="1488" />
            <wire x2="1920" y1="1664" y2="1664" x1="1488" />
            <wire x2="1712" y1="944" y2="944" x1="1488" />
            <wire x2="1712" y1="944" y2="976" x1="1712" />
            <wire x2="1936" y1="976" y2="976" x1="1712" />
        </branch>
        <branch name="c">
            <wire x2="1552" y1="656" y2="1072" x1="1552" />
            <wire x2="1552" y1="1072" y2="1536" x1="1552" />
            <wire x2="1920" y1="1536" y2="1536" x1="1552" />
            <wire x2="1552" y1="1536" y2="1904" x1="1552" />
            <wire x2="1552" y1="1904" y2="2080" x1="1552" />
            <wire x2="1920" y1="1904" y2="1904" x1="1552" />
            <wire x2="1744" y1="1072" y2="1072" x1="1552" />
            <wire x2="1744" y1="1072" y2="1104" x1="1744" />
            <wire x2="1936" y1="1104" y2="1104" x1="1744" />
        </branch>
        <iomarker fontsize="28" x="1488" y="656" name="a" orien="R270" />
        <iomarker fontsize="28" x="1520" y="848" name="b" orien="R270" />
        <iomarker fontsize="28" x="1552" y="656" name="c" orien="R270" />
        <iomarker fontsize="28" x="1584" y="848" name="d" orien="R270" />
        <instance x="1936" y="1168" name="XLXI_9" orien="R0" />
        <branch name="XLXN_19">
            <wire x2="2320" y1="1040" y2="1040" x1="2192" />
            <wire x2="2320" y1="1040" y2="1360" x1="2320" />
            <wire x2="2464" y1="1360" y2="1360" x1="2320" />
            <wire x2="2472" y1="1360" y2="1360" x1="2464" />
            <wire x2="2512" y1="1360" y2="1360" x1="2472" />
            <wire x2="2608" y1="1360" y2="1360" x1="2512" />
            <wire x2="2480" y1="1248" y2="1248" x1="2464" />
            <wire x2="2464" y1="1248" y2="1360" x1="2464" />
        </branch>
        <iomarker fontsize="28" x="2480" y="1248" name="XLXN_19" orien="R0" />
        <iomarker fontsize="28" x="2464" y="1632" name="XLXN_16" orien="R90" />
        <iomarker fontsize="28" x="2192" y="1488" name="XLXN_15" orien="R180" />
        <iomarker fontsize="28" x="2192" y="1424" name="XLXN_14" orien="R180" />
        <branch name="f_out">
            <wire x2="2960" y1="1456" y2="1456" x1="2864" />
            <wire x2="3008" y1="1456" y2="1456" x1="2960" />
            <wire x2="3024" y1="1456" y2="1456" x1="3008" />
        </branch>
        <iomarker fontsize="28" x="3024" y="1456" name="f_out" orien="R0" />
        <instance x="2608" y="1616" name="XLXI_18" orien="R0" />
        <branch name="XLXN_22">
            <wire x2="1200" y1="1232" y2="1232" x1="1184" />
        </branch>
    </sheet>
</drawing>