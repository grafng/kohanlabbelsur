-- Vhdl test bench created from schematic C:\Users\qwerty\Desktop\local vhdl\lab1\shemnew\shem.sch - Thu Jun 21 07:17:04 2018
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY shem_shem_sch_tb IS
END shem_shem_sch_tb;
ARCHITECTURE behavioral OF shem_shem_sch_tb IS 

   COMPONENT shem
   PORT( b	:	IN	STD_LOGIC; 
          d	:	IN	STD_LOGIC; 
          XLXN_14	:	OUT	STD_LOGIC; 
          XLXN_15	:	OUT	STD_LOGIC; 
          XLXN_16	:	OUT	STD_LOGIC; 
          a	:	IN	STD_LOGIC; 
          f_out	:	OUT	STD_LOGIC; 
          c	:	IN	STD_LOGIC; 
          XLXN_19	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL b	:	STD_LOGIC;
   SIGNAL d	:	STD_LOGIC;
   SIGNAL XLXN_14	:	STD_LOGIC;
   SIGNAL XLXN_15	:	STD_LOGIC;
   SIGNAL XLXN_16	:	STD_LOGIC;
   SIGNAL a	:	STD_LOGIC;
   SIGNAL f_out	:	STD_LOGIC;
   SIGNAL c	:	STD_LOGIC;
   SIGNAL XLXN_19	:	STD_LOGIC;

BEGIN


  front1: process is
  begin
  
    a <= '0';
    wait for 80 NS;
    a <= '1';
    wait for 80 NS;
	 a <= '0';
	 
  end process front1;



  front2: process is
  begin
    
    b <= '0';
    wait for 40 NS;
    b <= '1';
    wait for 40 NS;
    b <= '0';

  end process front2;


  
  front3: process is
  begin
    
    c <= '0';
    wait for 20 NS;
    c <= '1';
    wait for 20 NS;
    c <= '0';
    
  end process front3;


  
  front4: process is
  begin
    
    d <= '0';
    wait for 10 NS;
    d <= '1';
    wait for 10 NS;  
    d <= '0';
	 
  end process front4;




   UUT: shem PORT MAP(
		b => b, 
		d => d, 
		XLXN_14 => XLXN_14, 
		XLXN_15 => XLXN_15, 
		XLXN_16 => XLXN_16, 
		a => a, 
		f_out => f_out, 
		c => c, 
		XLXN_19 => XLXN_19
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
