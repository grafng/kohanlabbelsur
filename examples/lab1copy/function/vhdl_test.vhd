


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


entity regress_tb is
end regress_tb;

architecture behavior of regress_tb is 



  
  component regress
    port(
      x : in  std_logic_vector (16 downto 0);
      f : out  std_logic_vector (16 downto 0)
      );
  end component;
  
  
  signal x: std_logic_vector (16 downto 0);
  signal f: std_logic_vector (16 downto 0);
  constant CLK_period : time := 10 ns;
  
begin
  



  front1: process 
  begin
    x <= '0';
    wait for 10 NS;
    x <= '0000 0000 0000 0001';
    wait for 10 NS;  
    x <= '0';
  end process front1;

  front2: process 
  begin
    x <= '0';
    wait for 20 NS;
    x <= '0000 0000 0000 0010';
    wait for 20 NS;  
    x <= '0';
  end process front2;
  
  front3: process 
  begin
    x <= '0';
    wait for 30 NS;
    x <= '0000 0000 0000 0100';
    wait for 30 NS;  
    x <= '0';
  end process front3;
  
  front4: process 
  begin
    x <= '0';
    wait for 40 NS;
    x <= '0000 0000 0000 1000';
    wait for 40 NS;  
    x <= '0';
  end process front4;
  
  front5: process 
  begin
    x <= '0';
    wait for 50 NS;
    x <= '0000 0000 0001 0000';
    wait for 50 NS;  
    x <= '0';
  end process front5;
  
  front6: process 
  begin
    x <= '0';
    wait for 60 NS;
    x <= '0000 0000 0010 0000';
    wait for 60 NS;  
    x <= '0';
  end process front6;
  
  front7: process 
  begin
    x <= '0';
    wait for 70 NS;
    x <= '0000 0000 0100 0000';
    wait for 70 NS;  
    x <= '0';
  end process front7;
  
  front8: process 
  begin
    x <= '0';
    wait for 80 NS;
    x <= '0000 0000 1000 0000';
    wait for 80 NS;  
    x <= '0';
  end process front8;
  
  front9: process 
  begin
    x <= '0';
    wait for 90 NS;
    x <= '0000 0001 0000 0100';
    wait for 90 NS;  
    x <= '0';
  end process front9;
  
  front10: process 
  begin
    x <= '0';
    wait for 100 NS;
    x <= '0000 0010 0000 0000';
    wait for 100 NS;  
    x <= '0';
  end process front10;
  
  front11: process 
  begin
    x <= '0';
    wait for 110 NS;
    x <= '0000 0100 0000 0000';
    wait for 110 NS;  
    x <= '0';
  end process front110;
  
  front120: process 
  begin
    x <= '0';
    wait for 120 NS;
    x <= '0000 1000 0000 0000';
    wait for 120 NS;  
    x <= '0';
  end process front120;
  
  front130: process 
  begin
    x <= '0';
    wait for 130 NS;
    x <= '0001 0000 0000 0000';
    wait for 130 NS;  
    x <= '0';
  end process front130;
  
  front140: process 
  begin
    x <= '0';
    wait for 140 NS;
    x <= '0010 0000 0000 0000';
    wait for 140 NS;  
    x <= '0';
  end process front140;
  
  front150: process 
  begin
    x <= '0';
    wait for 150 NS;
    x <= '0100 0000 0000 0000';
    wait for 150 NS;  
    x <= '0';
  end process front150;
  
  front160: process 
  begin
    x <= '0';
    wait for 160 NS;
    x <= '1000 0000 0000 0000';
    wait for 160 NS;  
    x <= '0';
  end process front160;
  



  uut: regress port map (
    x => x,
    f => f
    );


end;
