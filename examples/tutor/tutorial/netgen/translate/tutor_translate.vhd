--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: tutor_translate.vhd
-- /___/   /\     Timestamp: Wed Jun 20 14:21:08 2018
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -tpw 0 -ar Structure -tm tutor -w -dir netgen/translate -ofmt vhdl -sim tutor.ngd tutor_translate.vhd 
-- Device	: 3s500efg320-4
-- Input file	: tutor.ngd
-- Output file	: C:\Users\qwerty\Desktop\local vhdl\examples\tutor\tutorial\netgen\translate\tutor_translate.vhd
-- # of Entities	: 1
-- Design Name	: tutor
-- Xilinx	: E:\14.7\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity tutor is
  port (
    DIRECTION : in STD_LOGIC := 'X'; 
    CLOCK : in STD_LOGIC := 'X'; 
    COUNT_OUT : out STD_LOGIC_VECTOR ( 3 downto 0 ) 
  );
end tutor;

architecture Structure of tutor is
begin
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => GSR);
  NlwBlockTOC : X_TOC
    port map (O => GTS);

end Structure;

