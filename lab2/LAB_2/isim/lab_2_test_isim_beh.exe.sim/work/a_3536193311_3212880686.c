/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
extern char *STD_STANDARD;
static const char *ng1 = "G:/projects/local vhdl/lab2/LAB_2/lab_2.vhd";
extern char *IEEE_P_3499444699;



int work_a_3536193311_3212880686_sub_1448394269_4157701399(char *t1, int t2, int t3)
{
    char t4[248];
    char t5[16];
    char t9[8];
    char t15[8];
    int t0;
    char *t6;
    char *t7;
    char *t8;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    unsigned char t20;
    unsigned char t21;
    unsigned char t22;
    int t23;
    int t24;
    char *t25;
    char *t26;
    int t27;
    int t28;
    int t29;
    int t30;
    int t31;
    int t32;
    int t33;
    int t34;
    int t35;
    int t36;
    int t37;

LAB0:    t6 = (t4 + 4U);
    t7 = ((STD_STANDARD) + 384);
    t8 = (t6 + 88U);
    *((char **)t8) = t7;
    t10 = (t6 + 56U);
    *((char **)t10) = t9;
    xsi_type_set_default_value(t7, t9, 0);
    t11 = (t6 + 80U);
    *((unsigned int *)t11) = 4U;
    t12 = (t4 + 124U);
    t13 = ((STD_STANDARD) + 384);
    t14 = (t12 + 88U);
    *((char **)t14) = t13;
    t16 = (t12 + 56U);
    *((char **)t16) = t15;
    xsi_type_set_default_value(t13, t15, 0);
    t17 = (t12 + 80U);
    *((unsigned int *)t17) = 4U;
    t18 = (t5 + 4U);
    *((int *)t18) = t2;
    t19 = (t5 + 8U);
    *((int *)t19) = t3;
    t21 = (t2 < 0);
    if (t21 == 1)
        goto LAB5;

LAB6:    t20 = (unsigned char)0;

LAB7:    if (t20 != 0)
        goto LAB2;

LAB4:    t20 = (t2 < 0);
    if (t20 != 0)
        goto LAB8;

LAB10:    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t7 = (t8 + 0);
    *((int *)t7) = t2;

LAB9:    t20 = (t3 < 0);
    if (t20 != 0)
        goto LAB11;

LAB13:    t7 = (t12 + 56U);
    t8 = *((char **)t7);
    t7 = (t8 + 0);
    *((int *)t7) = t3;

LAB12:
LAB3:    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t23 = *((int *)t8);
    t7 = (t12 + 56U);
    t10 = *((char **)t7);
    t24 = *((int *)t10);
    t27 = xsi_vhdl_mod(t23, t24);
    t20 = (t27 == 0);
    if (t20 != 0)
        goto LAB14;

LAB16:    t7 = (t12 + 56U);
    t8 = *((char **)t7);
    t23 = *((int *)t8);
    t7 = (t6 + 56U);
    t10 = *((char **)t7);
    t24 = *((int *)t10);
    t7 = (t12 + 56U);
    t11 = *((char **)t7);
    t27 = *((int *)t11);
    t28 = xsi_vhdl_mod(t24, t27);
    t29 = (t23 / t28);
    t21 = (t29 < 2);
    if (t21 == 1)
        goto LAB20;

LAB21:    t7 = (t12 + 56U);
    t13 = *((char **)t7);
    t30 = *((int *)t13);
    t7 = (t6 + 56U);
    t14 = *((char **)t7);
    t31 = *((int *)t14);
    t7 = (t12 + 56U);
    t16 = *((char **)t7);
    t32 = *((int *)t16);
    t33 = xsi_vhdl_mod(t31, t32);
    t34 = xsi_vhdl_mod(t30, t33);
    t22 = (t34 == 0);
    t20 = t22;

LAB22:    if (t20 != 0)
        goto LAB17;

LAB19:    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t23 = *((int *)t8);
    t7 = (t12 + 56U);
    t10 = *((char **)t7);
    t24 = *((int *)t10);
    t27 = (t23 / t24);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t7 = (t11 + 0);
    *((int *)t7) = t27;

LAB18:
LAB15:    t21 = (t2 < 0);
    if (t21 == 1)
        goto LAB26;

LAB27:    t22 = (t3 < 0);
    t20 = t22;

LAB28:    if (t20 != 0)
        goto LAB23;

LAB25:
LAB24:    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t23 = *((int *)t8);
    t0 = t23;

LAB1:    return t0;
LAB2:    t23 = (-(1));
    t24 = (t2 * t23);
    t25 = (t6 + 56U);
    t26 = *((char **)t25);
    t25 = (t26 + 0);
    *((int *)t25) = t24;
    t23 = (-(1));
    t24 = (t3 * t23);
    t7 = (t12 + 56U);
    t8 = *((char **)t7);
    t7 = (t8 + 0);
    *((int *)t7) = t24;
    goto LAB3;

LAB5:    t22 = (t3 < 0);
    t20 = t22;
    goto LAB7;

LAB8:    t23 = (-(1));
    t24 = (t2 * t23);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t7 = (t8 + 0);
    *((int *)t7) = t24;
    goto LAB9;

LAB11:    t23 = (-(1));
    t24 = (t3 * t23);
    t7 = (t12 + 56U);
    t8 = *((char **)t7);
    t7 = (t8 + 0);
    *((int *)t7) = t24;
    goto LAB12;

LAB14:    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t28 = *((int *)t11);
    t7 = (t12 + 56U);
    t13 = *((char **)t7);
    t29 = *((int *)t13);
    t30 = (t28 / t29);
    t7 = (t6 + 56U);
    t14 = *((char **)t7);
    t7 = (t14 + 0);
    *((int *)t7) = t30;
    goto LAB15;

LAB17:    t7 = (t6 + 56U);
    t17 = *((char **)t7);
    t35 = *((int *)t17);
    t7 = (t12 + 56U);
    t25 = *((char **)t7);
    t36 = *((int *)t25);
    t37 = (t35 / t36);
    t7 = (t6 + 56U);
    t26 = *((char **)t7);
    t7 = (t26 + 0);
    *((int *)t7) = t37;
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t23 = *((int *)t8);
    t24 = (t23 + 1);
    t7 = (t6 + 56U);
    t10 = *((char **)t7);
    t7 = (t10 + 0);
    *((int *)t7) = t24;
    goto LAB18;

LAB20:    t20 = (unsigned char)1;
    goto LAB22;

LAB23:    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t23 = *((int *)t8);
    t24 = (-(1));
    t27 = (t23 * t24);
    t7 = (t6 + 56U);
    t10 = *((char **)t7);
    t7 = (t10 + 0);
    *((int *)t7) = t27;
    goto LAB24;

LAB26:    t20 = (unsigned char)1;
    goto LAB28;

LAB29:;
}

static void work_a_3536193311_3212880686_p_0(char *t0)
{
    char t22[16];
    char *t1;
    char *t2;
    int t3;
    char *t4;
    char *t5;
    int t6;
    int t7;
    int t8;
    int t9;
    char *t10;
    int t11;
    int t12;
    char *t13;
    int t14;
    int t15;
    char *t16;
    int t17;
    int t18;
    int t19;
    int t20;
    char *t21;

LAB0:    xsi_set_current_line(70, ng1);
    t1 = (t0 + 1728U);
    t2 = *((char **)t1);
    t1 = (t2 + 0);
    *((int *)t1) = 4194304;
    xsi_set_current_line(72, ng1);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 4792U);
    t3 = ieee_std_logic_arith_conv_integer_unsigned(IEEE_P_3499444699, t2, t1);
    t4 = (t0 + 1488U);
    t5 = *((char **)t4);
    t4 = (t5 + 0);
    *((int *)t4) = t3;
    xsi_set_current_line(75, ng1);
    t1 = (t0 + 1488U);
    t2 = *((char **)t1);
    t3 = *((int *)t2);
    t1 = (t0 + 1488U);
    t4 = *((char **)t1);
    t6 = *((int *)t4);
    t7 = (t3 * t6);
    t1 = (t0 + 1488U);
    t5 = *((char **)t1);
    t8 = *((int *)t5);
    t9 = (t7 * t8);
    t1 = (t0 + 1488U);
    t10 = *((char **)t1);
    t11 = *((int *)t10);
    t12 = (t9 * t11);
    t1 = (t0 + 1488U);
    t13 = *((char **)t1);
    t14 = *((int *)t13);
    t15 = (t12 * t14);
    t1 = (t0 + 1488U);
    t16 = *((char **)t1);
    t17 = *((int *)t16);
    t18 = (t15 * t17);
    t19 = (-(71723));
    t20 = (t18 * t19);
    t1 = (t0 + 1608U);
    t21 = *((char **)t1);
    t1 = (t21 + 0);
    *((int *)t1) = t20;
    xsi_set_current_line(76, ng1);
    t1 = (t0 + 1608U);
    t2 = *((char **)t1);
    t3 = *((int *)t2);
    t1 = (t0 + 1488U);
    t4 = *((char **)t1);
    t6 = *((int *)t4);
    t1 = (t0 + 1488U);
    t5 = *((char **)t1);
    t7 = *((int *)t5);
    t8 = (t6 * t7);
    t1 = (t0 + 1488U);
    t10 = *((char **)t1);
    t9 = *((int *)t10);
    t11 = (t8 * t9);
    t1 = (t0 + 1488U);
    t13 = *((char **)t1);
    t12 = *((int *)t13);
    t14 = (t11 * t12);
    t1 = (t0 + 1488U);
    t16 = *((char **)t1);
    t15 = *((int *)t16);
    t17 = (t14 * t15);
    t18 = (t17 * 2642831);
    t19 = (t3 + t18);
    t1 = (t0 + 1608U);
    t21 = *((char **)t1);
    t1 = (t21 + 0);
    *((int *)t1) = t19;
    xsi_set_current_line(77, ng1);
    t1 = (t0 + 1608U);
    t2 = *((char **)t1);
    t3 = *((int *)t2);
    t1 = (t0 + 1488U);
    t4 = *((char **)t1);
    t6 = *((int *)t4);
    t1 = (t0 + 1488U);
    t5 = *((char **)t1);
    t7 = *((int *)t5);
    t8 = (t6 * t7);
    t1 = (t0 + 1488U);
    t10 = *((char **)t1);
    t9 = *((int *)t10);
    t11 = (t8 * t9);
    t1 = (t0 + 1488U);
    t13 = *((char **)t1);
    t12 = *((int *)t13);
    t14 = (t11 * t12);
    t15 = (-(37328467));
    t17 = (t14 * t15);
    t18 = (t3 + t17);
    t1 = (t0 + 1608U);
    t16 = *((char **)t1);
    t1 = (t16 + 0);
    *((int *)t1) = t18;
    xsi_set_current_line(78, ng1);
    t1 = (t0 + 1608U);
    t2 = *((char **)t1);
    t3 = *((int *)t2);
    t1 = (t0 + 1488U);
    t4 = *((char **)t1);
    t6 = *((int *)t4);
    t1 = (t0 + 1488U);
    t5 = *((char **)t1);
    t7 = *((int *)t5);
    t8 = (t6 * t7);
    t1 = (t0 + 1488U);
    t10 = *((char **)t1);
    t9 = *((int *)t10);
    t11 = (t8 * t9);
    t12 = (t11 * 253281436);
    t14 = (t3 + t12);
    t1 = (t0 + 1608U);
    t13 = *((char **)t1);
    t1 = (t13 + 0);
    *((int *)t1) = t14;
    xsi_set_current_line(79, ng1);
    t1 = (t0 + 1608U);
    t2 = *((char **)t1);
    t3 = *((int *)t2);
    t1 = (t0 + 1488U);
    t4 = *((char **)t1);
    t6 = *((int *)t4);
    t1 = (t0 + 1488U);
    t5 = *((char **)t1);
    t7 = *((int *)t5);
    t8 = (t6 * t7);
    t9 = (-(842006528));
    t11 = (t8 * t9);
    t12 = (t3 + t11);
    t1 = (t0 + 1608U);
    t10 = *((char **)t1);
    t1 = (t10 + 0);
    *((int *)t1) = t12;
    xsi_set_current_line(80, ng1);
    t1 = (t0 + 1608U);
    t2 = *((char **)t1);
    t3 = *((int *)t2);
    t1 = (t0 + 1488U);
    t4 = *((char **)t1);
    t6 = *((int *)t4);
    t7 = (t6 * 1240717066);
    t8 = (t3 + t7);
    t1 = (t0 + 1608U);
    t5 = *((char **)t1);
    t1 = (t5 + 0);
    *((int *)t1) = t8;
    xsi_set_current_line(81, ng1);
    t1 = (t0 + 1608U);
    t2 = *((char **)t1);
    t3 = *((int *)t2);
    t6 = (-(478318428));
    t7 = (t3 + t6);
    t1 = (t0 + 1608U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t7;
    xsi_set_current_line(83, ng1);
    t1 = (t0 + 1608U);
    t2 = *((char **)t1);
    t3 = *((int *)t2);
    t1 = (t0 + 1728U);
    t4 = *((char **)t1);
    t6 = *((int *)t4);
    t7 = work_a_3536193311_3212880686_sub_1448394269_4157701399(t0, t3, t6);
    t1 = (t0 + 1608U);
    t5 = *((char **)t1);
    t1 = (t5 + 0);
    *((int *)t1) = t7;
    xsi_set_current_line(85, ng1);
    t1 = (t0 + 1608U);
    t2 = *((char **)t1);
    t3 = *((int *)t2);
    t1 = ieee_std_logic_arith_conv_unsigned_integer(IEEE_P_3499444699, t22, t3, 8);
    t4 = (t0 + 3112);
    t5 = (t4 + 56U);
    t10 = *((char **)t5);
    t13 = (t10 + 56U);
    t16 = *((char **)t13);
    memcpy(t16, t1, 8U);
    xsi_driver_first_trans_fast_port(t4);
    t1 = (t0 + 3032);
    *((int *)t1) = 1;

LAB1:    return;
}


extern void work_a_3536193311_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3536193311_3212880686_p_0};
	static char *se[] = {(void *)work_a_3536193311_3212880686_sub_1448394269_4157701399};
	xsi_register_didat("work_a_3536193311_3212880686", "isim/lab_2_test_isim_beh.exe.sim/work/a_3536193311_3212880686.didat");
	xsi_register_executes(pe);
	xsi_register_subprogram_executes(se);
}
