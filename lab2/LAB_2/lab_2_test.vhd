






LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 



 
ENTITY lab_2_test IS
END lab_2_test;
 
ARCHITECTURE behavior OF lab_2_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT lab_2
    PORT(
         in1 : IN  std_logic_vector(3 downto 0);
         out1 : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal in1 : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal out1 : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
	
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: lab_2 PORT MAP (
          in1 => in1,
          out1 => out1
        );

   -- Stimulus process
   stim_proc: process
   		begin
				in1 <= X"1";
				wait for 100ns;
				in1 <= X"2";
				wait for 100ns;
				in1 <= X"3";
				wait for 100ns;
				in1 <= X"4";
				wait for 100ns;
				in1 <= X"5";
				wait for 100ns;
				in1 <= X"6";
				wait for 100ns;
				in1 <= X"7";
				wait for 100ns;
				in1 <= X"8";
				wait for 100ns;
				in1 <= X"9";
				wait for 100ns;
				in1 <= X"A";
				wait for 100ns;
				in1 <= X"B";
				wait for 100ns;
				in1 <= X"C";
				wait;
   end process;

END;