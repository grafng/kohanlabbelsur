




library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;




entity lab_2 is
  Port ( in1 : in  STD_LOGIC_VECTOR (3 downto 0);
         out1 : out  STD_LOGIC_VECTOR (7 downto 0));
end lab_2;

architecture Behavioral of lab_2 is
  
begin
  process (in1)
    --������� � �����������
    function del(a : integer; b : integer) return natural is
      variable y : integer;
      variable y1 : integer;
    begin
      --�������������� ������������� ����� � �������������
      if((a<0) and (b<0)) then
        y := a*(-1);
        y1 := b*(-1);
      else
        if(a<0) then
          y := a*(-1);
        else
          y := a;
        end if;	
        if(b<0) then
          y1 := b*(-1);
        else
          y1 := b;	
        end if;		
      end if;
      --���� ������� ����� ����, �� ���������� �� ����������  
      if((y mod y1)=0) then
        y:=y/y1;
      else
                                        --���� ��������� �������� � ������� ������ ���� ����� ����, �� ������� ���������� ��������� � ������� ������� , �.�. ��������� � ��� ������� 
        if((y1/(y mod y1)<2) or (y1 mod (y mod y1)=0)) then
          y:=y/y1;
          y:=y+1;
                                        --� ��������� ������ ���������� ��������� � ������� �������,�� ��� ��� ��� ������� ���� ����� � �������������� ������ ������� ����� ����������, �� ��������� ���� ��� ��� ������� ������ ����	
        else
          y:=y/y1;
        end if;
      end if;
					--�������������� ������ (���� �����-���� �� ������� ����� ����� ���� �������������) � ������������� �����
      if((a<0) or (b<0)) then
        y := y*(-1);
      end if;	
      return y;
    end del;
    
    --���������� �������� ����������	
    variable x : integer;
    variable y : integer;
    variable y1 : integer;
  --������ � ������������� ���������
  begin
  
    y1 := 4194304; 
    --������� ��������� ����� � ������������� ����������
    x := conv_integer(unsigned(in1));

	 --������ �������������� �������
	 y := ((x*x*x*x*x*x)*(-71723));
	 y := y+((x*x*x*x*x)*(2642831));
	 y := y+((x*x*x*x)*(-37328467));
	 y := y+((x*x*x)*253281436);
	 y := y+((x*x)*(-842006528));
	 y := y+(x*1240717066);
	 y := y+(-478318428);
	 --������� � �����������
	 y:=del(y,y1);
	 -- ������� � �������� 8 ������ �����
	 out1 <= std_logic_vector( conv_unsigned( y, 8 ) );

  end process;
end Behavioral;