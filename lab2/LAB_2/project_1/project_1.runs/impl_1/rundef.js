//
// PlanAhead(TM)
// rundef.js: a PlanAhead-generated Runs Script for WSH 5.1/5.6
// Copyright 1986-1999, 2001-2013 Xilinx, Inc. All Rights Reserved.
//

var WshShell = new ActiveXObject( "WScript.Shell" );
var ProcEnv = WshShell.Environment( "Process" );
var PathVal = ProcEnv("PATH");
if ( PathVal.length == 0 ) {
  PathVal = "E:/14.7/14.7/ISE_DS/EDK/bin/nt64;E:/14.7/14.7/ISE_DS/EDK/lib/nt64;E:/14.7/14.7/ISE_DS/ISE/bin/nt64;E:/14.7/14.7/ISE_DS/ISE/lib/nt64;E:/14.7/14.7/ISE_DS/common/bin/nt64;E:/14.7/14.7/ISE_DS/common/lib/nt64;E:/14.7/14.7/ISE_DS/PlanAhead/bin;";
} else {
  PathVal = "E:/14.7/14.7/ISE_DS/EDK/bin/nt64;E:/14.7/14.7/ISE_DS/EDK/lib/nt64;E:/14.7/14.7/ISE_DS/ISE/bin/nt64;E:/14.7/14.7/ISE_DS/ISE/lib/nt64;E:/14.7/14.7/ISE_DS/common/bin/nt64;E:/14.7/14.7/ISE_DS/common/lib/nt64;E:/14.7/14.7/ISE_DS/PlanAhead/bin;" + PathVal;
}

ProcEnv("PATH") = PathVal;

var RDScrFP = WScript.ScriptFullName;
var RDScrN = WScript.ScriptName;
var RDScrDir = RDScrFP.substr( 0, RDScrFP.length - RDScrN.length - 1 );
var ISEJScriptLib = RDScrDir + "/ISEWrap.js";
eval( EAInclude(ISEJScriptLib) );


ISEStep( "ngdbuild",
         "-intstyle ise -p xc6slx150fgg484-3 -dd _ngo -uc \"lab_2.ucf\" \"lab_2.edf\"" );
ISEStep( "map",
         "-intstyle pa -w -r 4 -ol high \"lab_2.ngd\"" );
ISEStep( "par",
         "-intstyle pa \"lab_2.ncd\" -w \"lab_2_routed.ncd\" -ol high" );
ISEStep( "trce",
         "-intstyle ise -o \"lab_2.twr\" -v 3 -l 30 -nodatasheet -fastpaths \"lab_2_routed.ncd\" \"lab_2.pcf\"" );
ISEStep( "xdl",
         "-secure -ncd2xdl -nopips \"lab_2_routed.ncd\" \"lab_2_routed.xdl\"" );



function EAInclude( EAInclFilename ) {
  var EAFso = new ActiveXObject( "Scripting.FileSystemObject" );
  var EAInclFile = EAFso.OpenTextFile( EAInclFilename );
  var EAIFContents = EAInclFile.ReadAll();
  EAInclFile.Close();
  return EAIFContents;
}
