
# PlanAhead Launch Script for Post PAR Floorplanning, created by Project Navigator

create_project -name LAB_2 -dir "D:/GROMYKO_I_L/LAB_2/planAhead_run_3" -part xc6slx150fgg484-3
set srcset [get_property srcset [current_run -impl]]
set_property design_mode GateLvl $srcset
set_property edif_top_file "D:/GROMYKO_I_L/LAB_2/lab_2.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {D:/GROMYKO_I_L/LAB_2} }
set_property target_constrs_file "lab_2.ucf" [current_fileset -constrset]
add_files [list {lab_2.ucf}] -fileset [get_property constrset [current_run]]
link_design
read_xdl -file "D:/GROMYKO_I_L/LAB_2/lab_2.ncd"
if {[catch {read_twx -name results_1 -file "D:/GROMYKO_I_L/LAB_2/lab_2.twx"} eInfo]} {
   puts "WARNING: there was a problem importing \"D:/GROMYKO_I_L/LAB_2/lab_2.twx\": $eInfo"
}
