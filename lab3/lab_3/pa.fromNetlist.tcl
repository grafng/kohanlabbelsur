
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name lab_3 -dir "D:/GROMYKO_I_L/LAB_3/lab_3/planAhead_run_3" -part xc6slx4tqg144-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "D:/GROMYKO_I_L/LAB_3/lab_3/alu.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {D:/GROMYKO_I_L/LAB_3/lab_3} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "alu.ucf" [current_fileset -constrset]
add_files [list {alu.ucf}] -fileset [get_property constrset [current_run]]
link_design
