/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
extern char *IEEE_P_3620187407;
static const char *ng1 = "E:/projects/local vhdl/lab3/lab_3/lab_3.vhd";
extern char *IEEE_P_2592010699;

char *ieee_p_2592010699_sub_1837678034_503743352(char *, char *, char *, char *);
unsigned char ieee_p_3620187407_sub_4042748798_3965413181(char *, char *, char *, char *, char *);
char *ieee_p_3620187407_sub_767632659_3965413181(char *, char *, char *, char *, char *, char *);


void work_a_0832606739_2730021763_sub_3161422046_888531987(char *t0, char *t1, char *t2, char *t3)
{
    char t5[32];
    char t6[16];
    char t19[16];
    char t25[16];
    char *t7;
    char *t8;
    int t9;
    unsigned int t10;
    unsigned char t11;
    char *t12;
    char *t13;
    char *t14;
    int t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;
    char *t20;
    char *t21;
    int t22;
    unsigned int t23;
    char *t26;
    char *t27;
    int t28;
    unsigned char t29;

LAB0:    t7 = (t6 + 0U);
    t8 = (t7 + 0U);
    *((int *)t8) = 15;
    t8 = (t7 + 4U);
    *((int *)t8) = 0;
    t8 = (t7 + 8U);
    *((int *)t8) = -1;
    t9 = (0 - 15);
    t10 = (t9 * -1);
    t10 = (t10 + 1);
    t8 = (t7 + 12U);
    *((unsigned int *)t8) = t10;
    t8 = (t5 + 4U);
    t11 = (t2 != 0);
    if (t11 == 1)
        goto LAB3;

LAB2:    t12 = (t5 + 12U);
    *((char **)t12) = t6;
    t13 = (t5 + 20U);
    *((char **)t13) = t3;
    t14 = (t6 + 0U);
    t15 = *((int *)t14);
    t10 = (t15 - 15);
    t16 = (t10 * 1U);
    t17 = (0 + t16);
    t18 = (t2 + t17);
    t20 = (t19 + 0U);
    t21 = (t20 + 0U);
    *((int *)t21) = 15;
    t21 = (t20 + 4U);
    *((int *)t21) = 8;
    t21 = (t20 + 8U);
    *((int *)t21) = -1;
    t22 = (8 - 15);
    t23 = (t22 * -1);
    t23 = (t23 + 1);
    t21 = (t20 + 12U);
    *((unsigned int *)t21) = t23;
    t21 = (t0 + 7504);
    t26 = (t25 + 0U);
    t27 = (t26 + 0U);
    *((int *)t27) = 0;
    t27 = (t26 + 4U);
    *((int *)t27) = 7;
    t27 = (t26 + 8U);
    *((int *)t27) = 1;
    t28 = (7 - 0);
    t23 = (t28 * 1);
    t23 = (t23 + 1);
    t27 = (t26 + 12U);
    *((unsigned int *)t27) = t23;
    t29 = ieee_p_3620187407_sub_4042748798_3965413181(IEEE_P_3620187407, t18, t19, t21, t25);
    if (t29 != 0)
        goto LAB4;

LAB6:    t7 = (t3 + 0);
    *((unsigned char *)t7) = (unsigned char)2;

LAB5:
LAB1:    return;
LAB3:    *((char **)t8) = t2;
    goto LAB2;

LAB4:    t27 = (t3 + 0);
    *((unsigned char *)t27) = (unsigned char)3;
    goto LAB5;

}

void work_a_0832606739_2730021763_sub_270288461_888531987(char *t0, char *t1, char *t2, char *t3)
{
    char t5[32];
    char t6[16];
    char t19[16];
    char t25[16];
    char *t7;
    char *t8;
    int t9;
    unsigned int t10;
    unsigned char t11;
    char *t12;
    char *t13;
    char *t14;
    int t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;
    char *t20;
    char *t21;
    int t22;
    unsigned int t23;
    char *t26;
    char *t27;
    int t28;
    unsigned char t29;

LAB0:    t7 = (t6 + 0U);
    t8 = (t7 + 0U);
    *((int *)t8) = 15;
    t8 = (t7 + 4U);
    *((int *)t8) = 0;
    t8 = (t7 + 8U);
    *((int *)t8) = -1;
    t9 = (0 - 15);
    t10 = (t9 * -1);
    t10 = (t10 + 1);
    t8 = (t7 + 12U);
    *((unsigned int *)t8) = t10;
    t8 = (t5 + 4U);
    t11 = (t2 != 0);
    if (t11 == 1)
        goto LAB3;

LAB2:    t12 = (t5 + 12U);
    *((char **)t12) = t6;
    t13 = (t5 + 20U);
    *((char **)t13) = t3;
    t14 = (t6 + 0U);
    t15 = *((int *)t14);
    t10 = (t15 - 7);
    t16 = (t10 * 1U);
    t17 = (0 + t16);
    t18 = (t2 + t17);
    t20 = (t19 + 0U);
    t21 = (t20 + 0U);
    *((int *)t21) = 7;
    t21 = (t20 + 4U);
    *((int *)t21) = 0;
    t21 = (t20 + 8U);
    *((int *)t21) = -1;
    t22 = (0 - 7);
    t23 = (t22 * -1);
    t23 = (t23 + 1);
    t21 = (t20 + 12U);
    *((unsigned int *)t21) = t23;
    t21 = (t0 + 7512);
    t26 = (t25 + 0U);
    t27 = (t26 + 0U);
    *((int *)t27) = 0;
    t27 = (t26 + 4U);
    *((int *)t27) = 7;
    t27 = (t26 + 8U);
    *((int *)t27) = 1;
    t28 = (7 - 0);
    t23 = (t28 * 1);
    t23 = (t23 + 1);
    t27 = (t26 + 12U);
    *((unsigned int *)t27) = t23;
    t29 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t18, t19, t21, t25);
    if (t29 != 0)
        goto LAB4;

LAB6:    t7 = (t3 + 0);
    *((unsigned char *)t7) = (unsigned char)2;

LAB5:
LAB1:    return;
LAB3:    *((char **)t8) = t2;
    goto LAB2;

LAB4:    t27 = (t3 + 0);
    *((unsigned char *)t27) = (unsigned char)3;
    goto LAB5;

}

static void work_a_0832606739_2730021763_p_0(char *t0)
{
    char t14[16];
    char t25[16];
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    int t11;
    int t12;
    int t13;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;

LAB0:    xsi_set_current_line(78, ng1);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)2);
    if (t4 != 0)
        goto LAB2;

LAB4:
LAB3:    xsi_set_current_line(117, ng1);
    t1 = (t0 + 3048U);
    t2 = *((char **)t1);
    t8 = (15 - 7);
    t9 = (t8 * 1U);
    t10 = (0 + t9);
    t1 = (t2 + t10);
    t5 = (t0 + 4792);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t15 = (t7 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 8U);
    xsi_driver_first_trans_fast_port(t5);
    xsi_set_current_line(119, ng1);
    t1 = (t0 + 3168U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 4856);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t15 = *((char **)t7);
    *((unsigned char *)t15) = t3;
    xsi_driver_first_trans_fast_port(t1);
    xsi_set_current_line(120, ng1);
    t1 = (t0 + 3408U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 4920);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t15 = *((char **)t7);
    *((unsigned char *)t15) = t3;
    xsi_driver_first_trans_fast_port(t1);
    t1 = (t0 + 4712);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(84, ng1);
    t1 = (t0 + 7520);
    t6 = (t0 + 2808U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t1, 16U);
    xsi_set_current_line(85, ng1);
    t1 = (t0 + 7536);
    t5 = (t0 + 2928U);
    t6 = *((char **)t5);
    t5 = (t6 + 0);
    memcpy(t5, t1, 16U);
    xsi_set_current_line(86, ng1);
    t1 = (t0 + 1352U);
    t2 = *((char **)t1);
    t1 = (t0 + 2808U);
    t5 = *((char **)t1);
    t8 = (15 - 7);
    t9 = (t8 * 1U);
    t10 = (0 + t9);
    t1 = (t5 + t10);
    memcpy(t1, t2, 8U);
    xsi_set_current_line(87, ng1);
    t1 = (t0 + 1512U);
    t2 = *((char **)t1);
    t1 = (t0 + 2928U);
    t5 = *((char **)t1);
    t8 = (15 - 7);
    t9 = (t8 * 1U);
    t10 = (0 + t9);
    t1 = (t5 + t10);
    memcpy(t1, t2, 8U);
    xsi_set_current_line(90, ng1);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t1 = (t0 + 2448U);
    t5 = *((char **)t1);
    t11 = xsi_mem_cmp(t5, t2, 4U);
    if (t11 == 1)
        goto LAB6;

LAB10:    t1 = (t0 + 2568U);
    t6 = *((char **)t1);
    t12 = xsi_mem_cmp(t6, t2, 4U);
    if (t12 == 1)
        goto LAB7;

LAB11:    t1 = (t0 + 2688U);
    t7 = *((char **)t1);
    t13 = xsi_mem_cmp(t7, t2, 4U);
    if (t13 == 1)
        goto LAB8;

LAB12:
LAB9:    xsi_set_current_line(109, ng1);
    t1 = (t0 + 7552);
    t5 = (t0 + 3048U);
    t6 = *((char **)t5);
    t5 = (t6 + 0);
    memcpy(t5, t1, 16U);

LAB5:    goto LAB3;

LAB6:    xsi_set_current_line(93, ng1);
    t1 = (t0 + 1352U);
    t15 = *((char **)t1);
    t1 = (t0 + 7284U);
    t16 = (t0 + 1512U);
    t17 = *((char **)t16);
    t16 = (t0 + 7300U);
    t18 = ieee_p_3620187407_sub_767632659_3965413181(IEEE_P_3620187407, t14, t15, t1, t17, t16);
    t19 = (t0 + 3048U);
    t20 = *((char **)t19);
    t19 = (t20 + 0);
    t21 = (t14 + 12U);
    t8 = *((unsigned int *)t21);
    t9 = (1U * t8);
    memcpy(t19, t18, t9);
    xsi_set_current_line(94, ng1);
    t1 = (t0 + 4200);
    t2 = (t0 + 3048U);
    t5 = *((char **)t2);
    memcpy(t14, t5, 16U);
    t2 = (t0 + 3408U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    work_a_0832606739_2730021763_sub_3161422046_888531987(t0, t1, t14, t2);
    goto LAB5;

LAB7:    xsi_set_current_line(98, ng1);
    t1 = (t0 + 2808U);
    t2 = *((char **)t1);
    t11 = (7 - 15);
    t8 = (t11 * -1);
    t9 = (1U * t8);
    t10 = (0 + t9);
    t1 = (t2 + t10);
    t3 = *((unsigned char *)t1);
    t5 = (t0 + 3048U);
    t6 = *((char **)t5);
    t12 = (0 - 15);
    t22 = (t12 * -1);
    t23 = (1U * t22);
    t24 = (0 + t23);
    t5 = (t6 + t24);
    *((unsigned char *)t5) = t3;
    xsi_set_current_line(99, ng1);
    t1 = (t0 + 2808U);
    t2 = *((char **)t1);
    t8 = (15 - 6);
    t9 = (t8 * 1U);
    t10 = (0 + t9);
    t1 = (t2 + t10);
    t5 = (t0 + 3048U);
    t6 = *((char **)t5);
    t22 = (15 - 7);
    t23 = (t22 * 1U);
    t24 = (0 + t23);
    t5 = (t6 + t24);
    memcpy(t5, t1, 7U);
    goto LAB5;

LAB8:    xsi_set_current_line(102, ng1);
    t1 = (t0 + 2808U);
    t2 = *((char **)t1);
    t1 = (t0 + 7380U);
    t5 = ieee_p_2592010699_sub_1837678034_503743352(IEEE_P_2592010699, t25, t2, t1);
    t6 = (t0 + 3048U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    t15 = (t25 + 12U);
    t8 = *((unsigned int *)t15);
    t9 = (1U * t8);
    memcpy(t6, t5, t9);
    xsi_set_current_line(103, ng1);
    t1 = (t0 + 4200);
    t2 = (t0 + 3048U);
    t5 = *((char **)t2);
    memcpy(t25, t5, 16U);
    t2 = (t0 + 3168U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    work_a_0832606739_2730021763_sub_270288461_888531987(t0, t1, t25, t2);
    goto LAB5;

LAB13:;
}


extern void work_a_0832606739_2730021763_init()
{
	static char *pe[] = {(void *)work_a_0832606739_2730021763_p_0};
	static char *se[] = {(void *)work_a_0832606739_2730021763_sub_3161422046_888531987,(void *)work_a_0832606739_2730021763_sub_270288461_888531987};
	xsi_register_didat("work_a_0832606739_2730021763", "isim/alu_tb_isim_beh.exe.sim/work/a_0832606739_2730021763.didat");
	xsi_register_executes(pe);
	xsi_register_subprogram_executes(se);
}
