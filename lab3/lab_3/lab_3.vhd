
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ����� ���
-- reset - �����
-- op_code - ��� ��������
-- in1 - ������ �������
-- in2 - ������ �������
-- out1 - ���������
-- flag_z - ���� �������� ����������
-- flag_cy - ���� ��������
-- flag_ov - ���� ������������

entity alu is
	port (
		reset   : in std_logic;
		op_code : in std_logic_vector (3 downto 0);
		in1     : in std_logic_vector (7 downto 0);
		in2     : in std_logic_vector (7 downto 0);
		out1    : out std_logic_vector (7 downto 0);
		flag_z  : out std_logic;
		flag_cy : out std_logic;
		flag_ov : out std_logic);
end alu;

architecture behaviour of alu is
	--���� ��������, ������� ��������� ���
	constant alu_op_mul : std_logic_vector (3 downto 0) := "0001";
	constant alu_op_rol : std_logic_vector (3 downto 0) := "0010";
	constant alu_op_not : std_logic_vector (3 downto 0) := "0011";

	-- ��������� ����������� �� �������� ���������� �������� ����� OV
	procedure calc_overflow_flag (
		result        : in std_logic_vector (15 downto 0);
		overflow_flag : out std_logic) is
	begin
		-- ����������� ������� 8 ��� ����������
		if (result(15 downto 8) /= "00000000") then
			-- ���� ��� �� �������, �� ���������� ���� ������������
			overflow_flag := '1';
		else
			-- ���� ��� �������, �� �������� ���� ������������
			overflow_flag := '0';
		end if;
	end calc_overflow_flag;
	
	
	-- ��������� ����������� �� �������� ���������� �������� ����� Z
	procedure z_flag (
		result    : in std_logic_vector (15 downto 0);
		zero_flag : out std_logic) is
	begin
		if (result(7 downto 0) = "00000000") then
			zero_flag := '1';
		else
			zero_flag := '0';
		end if;
	end z_flag;
	
	
begin
	process (
		reset,
		op_code,
		in1,
		in2)
		-- ��������� ���������� ��� �������� �������� ������ ��������
		variable tmp_in1, tmp_in2      : std_logic_vector (15 downto 0);
		variable res                   : std_logic_vector (15 downto 0);
		variable res_z, res_cy, res_ov : std_logic;
	
	begin
	
	
		if reset = '0' then
			-- �������� ���������� ���������� � ���������� � ���
			-- ������� ��������
			-- ���������� ���������� ����� ����� ������� �����������, ���
			-- �������, ��� ����, ��� �� ����� ����������� �������������
			-- �����
			tmp_in1             := "0000000000000000";
			tmp_in2             := "0000000000000000";
			tmp_in1(7 downto 0) := in1;
			tmp_in2(7 downto 0) := in2;
			
			-- ���������� ����� ��� �������� ����������
			case op_code is
			
				when alu_op_mul =>
					res := in1 * in2;
					calc_overflow_flag(res, res_ov);
					
				when alu_op_rol =>
					-- ����������� ����� �����
					res(0)          := tmp_in1(7);
					res(7 downto 1) := tmp_in1(6 downto 0);

				when alu_op_not =>
					res := not tmp_in1;
					z_flag(res, res_z);



				when others =>
					-- ����������� ��� ��������
					res := "ZZZZZZZZZZZZZZZZ";

			end case;

		end if;

		-- ������ � ����� 8 ������� ��� ���������� �
		-- �������� ������
		out1    <= res(7 downto 0);
		
		flag_z  <= res_z;
		flag_ov <= res_ov;

	end process;

end behaviour;