



library ieee;
use ieee.std_logic_arith.all;
use ieee.std_logic_1164.all;

-- Add your library and packages declaration here ...

entity alu_tb is
end alu_tb;

architecture TB_ARCHITECTURE of alu_tb is

	--���� ��������, ������� ��������� ���
	constant alu_op_mul : std_logic_vector (3 downto 0) := "0001";
	constant alu_op_rol : std_logic_vector (3 downto 0) := "0010";
	constant alu_op_not : std_logic_vector (3 downto 0) := "0011";


	component alu
		port (
			reset   : in std_logic;
			op_code : in std_logic_vector(3 downto 0);
			in1     : in std_logic_vector(7 downto 0);
			in2     : in std_logic_vector(7 downto 0);
			out1    : out std_logic_vector(7 downto 0);
			flag_z  : out std_logic;
			flag_ov : out std_logic);
	end component;

	-- Stimulus signals - signals mapped to the input and inout ports of tested entity
	signal reset   : std_logic;
	signal op_code : std_logic_vector(3 downto 0);
	signal in1     : std_logic_vector(7 downto 0);
	signal in2     : std_logic_vector(7 downto 0);

	-- Observed signals - signals mapped to the output ports of tested entity
	signal out1    : std_logic_vector(7 downto 0);
	signal flag_z  : std_logic;
	signal flag_ov : std_logic;
	
begin

	-- Unit Under Test port map
	UUT : alu
	port map(
		reset   => reset,
		op_code => op_code,
		in1     => in1,
		in2     => in2,
		out1    => out1,
		flag_z  => flag_z,
		flag_ov => flag_ov
	);

	-- Add your stimulus here ...
	-- �������, ������������ �������� ������������������
	test_data_generator :
	process
		variable result       : std_logic_vector (7 downto 0);
		variable right_result : std_logic_vector (7 downto 0);

	begin

		-- ������ Reset �� ��� -- ���� �� ����������� ������� ��������
		reset   <= '1';

		-- ������������ ��������� ��� ������������
		op_code <= alu_op_mul;
		
		in1     <= "00010100";
		in2     <= "00001100";
		
		reset   <= '0';
		wait for 50ns;
		result       := out1;
		right_result := "11110000";
		assert (result = right_result) and (flag_ov = '0')
		report "��������� ��� ������������ ����������� �����������"
			severity failure;
		reset   <= '1';


		-- ������������ ��������� c �������������
		op_code <= alu_op_mul;
		in1     <= "00110100";
		in2     <= "00001100";
		reset   <= '0';
		wait for 50ns;
		result       := out1;
		right_result := "01110000";
		assert (result = right_result) and (flag_ov = '1')
		report "��������� � ������������� ����������� �����������"
			severity failure;
		reset   <= '1';

		-- ������������ ������������ ������ �����
		op_code <= alu_op_rol;
		in1     <= "10110100";
		reset   <= '0';
		wait for 50ns;
		result       := out1;
		right_result := "01101001";
		assert (result = right_result)
		report "����������� ����� ����� ����������� �����������"
			severity failure;
		reset <= '1';


		-- ������������ ���������� �� ��� ��������� ����� Z
		op_code <= alu_op_not;
		-- �������� ����� ������ ���� �������. �������� in2
		-- �������������
		in1     <= "00110100";
		reset   <= '0';
		wait for 50ns;
		result       := out1;
		right_result := "11001011";
		assert (result = right_result) and (flag_z = '0')
		report "��������� �� ��� ��������� ����� Z ����������� �����������"
			severity failure;
		reset   <= '1';


			
		-- ������������ ���������� �� � ���������� ����� Z
		op_code <= alu_op_not;
		in1     <= "11111111";
		reset   <= '0';
		wait for 50ns;
		result       := out1;
		right_result := "00000000";
		assert (result = right_result) and (flag_z = '1')
		report "��������� �� � ���������� ����� Z ����������� �����������"
			severity failure;
		reset   <= '1';

			
		report "��� ����� ��������! ����������! :-)";
		
		wait;

	end process;

end TB_ARCHITECTURE;

configuration TESTBENCH_FOR_alu of alu_tb is
	for TB_ARCHITECTURE
		for UUT : alu
			use entity work.alu(behaviour);
		end for;
	end for;
end TESTBENCH_FOR_alu;