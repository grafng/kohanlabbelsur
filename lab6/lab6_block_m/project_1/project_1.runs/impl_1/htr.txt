REM
REM PlanAhead(TM)
REM htr.txt: a PlanAhead-generated description of how-to-repeat the
REM          the basic steps of a run.  Note that runme.bat/sh needs
REM          to be invoked for PlanAhead to track run status.
REM Copyright 1986-1999, 2001-2013 Xilinx, Inc. All Rights Reserved.
REM

ngdbuild -intstyle ise -p xc7vx330tffg1157-3 -dd _ngo -uc "stack.ucf" "stack.edf"
map -intstyle pa -w -r 4 -ol high  -ntd "stack.ngd"
par -intstyle pa "stack.ncd" -w "stack_routed.ncd" -ol high
trce -intstyle ise -o "stack.twr" -v 3 -l 30 -nodatasheet -fastpaths "stack_routed.ncd" "stack.pcf"
xdl -secure -ncd2xdl -nopips "stack_routed.ncd" "stack_routed.xdl"
