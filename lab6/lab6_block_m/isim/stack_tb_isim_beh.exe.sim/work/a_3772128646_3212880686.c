/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "G:/projects/local vhdl/lab6/lab6_block_m/stack.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );


static void work_a_3772128646_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(149, ng0);

LAB3:    t1 = (t0 + 6008U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 15976);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t8 = (t0 + 15864);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3772128646_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(150, ng0);

LAB3:    t1 = (t0 + 6168U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 16040);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t8 = (t0 + 15880);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3772128646_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    unsigned char t10;
    unsigned char t11;
    char *t12;
    unsigned char t13;
    unsigned char t14;
    unsigned char t15;
    unsigned char t16;
    int t17;
    unsigned char t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    int t24;

LAB0:    xsi_set_current_line(156, ng0);
    t1 = (t0 + 4888U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 4688U);
    t3 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t3 != 0)
        goto LAB5;

LAB6:
LAB3:    t1 = (t0 + 15896);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(157, ng0);
    t1 = (t0 + 14064U);
    t5 = *((char **)t1);
    t1 = (t5 + 0);
    *((int *)t1) = 13;
    xsi_set_current_line(158, ng0);
    t1 = (t0 + 16104);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(159, ng0);
    t1 = (t0 + 16168);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(160, ng0);
    t1 = xsi_get_transient_memory(16U);
    memset(t1, 0, 16U);
    t2 = t1;
    memset(t2, (unsigned char)2, 16U);
    t5 = (t0 + 16232);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t5);
    xsi_set_current_line(161, ng0);
    t1 = (t0 + 16296);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    goto LAB3;

LAB5:    xsi_set_current_line(165, ng0);
    t2 = (t0 + 5048U);
    t5 = *((char **)t2);
    t4 = *((unsigned char *)t5);
    t10 = (t4 == (unsigned char)3);
    if (t10 != 0)
        goto LAB7;

LAB9:    xsi_set_current_line(168, ng0);
    t1 = (t0 + 16296);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);

LAB8:    xsi_set_current_line(172, ng0);
    t1 = (t0 + 5048U);
    t2 = *((char **)t1);
    t10 = *((unsigned char *)t2);
    t11 = (t10 == (unsigned char)3);
    if (t11 == 1)
        goto LAB16;

LAB17:    t4 = (unsigned char)0;

LAB18:    if (t4 == 1)
        goto LAB13;

LAB14:    t3 = (unsigned char)0;

LAB15:    if (t3 != 0)
        goto LAB10;

LAB12:
LAB11:    xsi_set_current_line(196, ng0);
    t1 = (t0 + 5048U);
    t2 = *((char **)t1);
    t10 = *((unsigned char *)t2);
    t11 = (t10 == (unsigned char)3);
    if (t11 == 1)
        goto LAB37;

LAB38:    t4 = (unsigned char)0;

LAB39:    if (t4 == 1)
        goto LAB34;

LAB35:    t3 = (unsigned char)0;

LAB36:    if (t3 != 0)
        goto LAB31;

LAB33:
LAB32:    xsi_set_current_line(219, ng0);
    t1 = (t0 + 14064U);
    t2 = *((char **)t1);
    t17 = *((int *)t2);
    t1 = (t0 + 16424);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((int *)t8) = t17;
    xsi_driver_first_trans_fast(t1);
    goto LAB3;

LAB7:    xsi_set_current_line(166, ng0);
    t2 = (t0 + 5528U);
    t6 = *((char **)t2);
    t11 = *((unsigned char *)t6);
    t2 = (t0 + 16296);
    t7 = (t2 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t12 = *((char **)t9);
    *((unsigned char *)t12) = t11;
    xsi_driver_first_trans_fast(t2);
    goto LAB8;

LAB10:    xsi_set_current_line(174, ng0);
    t1 = (t0 + 14064U);
    t7 = *((char **)t1);
    t17 = *((int *)t7);
    t18 = (t17 == 13);
    if (t18 != 0)
        goto LAB19;

LAB21:    xsi_set_current_line(178, ng0);
    t1 = (t0 + 16104);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(179, ng0);
    t1 = (t0 + 16168);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);

LAB20:    xsi_set_current_line(182, ng0);
    t1 = (t0 + 6328U);
    t2 = *((char **)t1);
    t4 = *((unsigned char *)t2);
    t10 = (t4 == (unsigned char)3);
    if (t10 == 1)
        goto LAB25;

LAB26:    t3 = (unsigned char)0;

LAB27:    if (t3 != 0)
        goto LAB22;

LAB24:
LAB23:    xsi_set_current_line(188, ng0);
    t1 = (t0 + 6968U);
    t2 = *((char **)t1);
    t1 = (t0 + 14064U);
    t5 = *((char **)t1);
    t17 = *((int *)t5);
    t20 = (t17 - 0);
    t21 = (t20 * 1);
    xsi_vhdl_check_range_of_index(0, 13, 1, t17);
    t22 = (16U * t21);
    t23 = (0 + t22);
    t1 = (t2 + t23);
    t6 = (t0 + 16232);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t12 = *((char **)t9);
    memcpy(t12, t1, 16U);
    xsi_driver_first_trans_fast_port(t6);
    xsi_set_current_line(190, ng0);
    t1 = (t0 + 14064U);
    t2 = *((char **)t1);
    t17 = *((int *)t2);
    t3 = (t17 != 13);
    if (t3 != 0)
        goto LAB28;

LAB30:
LAB29:    goto LAB11;

LAB13:    t1 = (t0 + 6168U);
    t6 = *((char **)t1);
    t15 = *((unsigned char *)t6);
    t16 = (t15 == (unsigned char)2);
    t3 = t16;
    goto LAB15;

LAB16:    t1 = (t0 + 5528U);
    t5 = *((char **)t1);
    t13 = *((unsigned char *)t5);
    t14 = (t13 == (unsigned char)2);
    t4 = t14;
    goto LAB18;

LAB19:    xsi_set_current_line(175, ng0);
    t1 = (t0 + 16104);
    t8 = (t1 + 56U);
    t9 = *((char **)t8);
    t12 = (t9 + 56U);
    t19 = *((char **)t12);
    *((unsigned char *)t19) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(176, ng0);
    t1 = (t0 + 16168);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    goto LAB20;

LAB22:    xsi_set_current_line(183, ng0);
    t1 = (t0 + 14064U);
    t6 = *((char **)t1);
    t17 = *((int *)t6);
    t20 = (t17 + 1);
    t1 = (t0 + 14064U);
    t7 = *((char **)t1);
    t1 = (t7 + 0);
    *((int *)t1) = t20;
    goto LAB23;

LAB25:    t1 = (t0 + 6008U);
    t5 = *((char **)t1);
    t11 = *((unsigned char *)t5);
    t13 = (t11 == (unsigned char)2);
    t3 = t13;
    goto LAB27;

LAB28:    xsi_set_current_line(191, ng0);
    t1 = (t0 + 14064U);
    t5 = *((char **)t1);
    t20 = *((int *)t5);
    t24 = (t20 + 1);
    t1 = (t0 + 14064U);
    t6 = *((char **)t1);
    t1 = (t6 + 0);
    *((int *)t1) = t24;
    goto LAB29;

LAB31:    xsi_set_current_line(198, ng0);
    t1 = (t0 + 14064U);
    t7 = *((char **)t1);
    t17 = *((int *)t7);
    t18 = (t17 == 0);
    if (t18 != 0)
        goto LAB40;

LAB42:    xsi_set_current_line(202, ng0);
    t1 = (t0 + 16104);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(203, ng0);
    t1 = (t0 + 16168);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);

LAB41:    xsi_set_current_line(206, ng0);
    t1 = (t0 + 6328U);
    t2 = *((char **)t1);
    t4 = *((unsigned char *)t2);
    t10 = (t4 == (unsigned char)2);
    if (t10 == 1)
        goto LAB46;

LAB47:    t3 = (unsigned char)0;

LAB48:    if (t3 != 0)
        goto LAB43;

LAB45:
LAB44:    xsi_set_current_line(212, ng0);
    t1 = (t0 + 5208U);
    t2 = *((char **)t1);
    t1 = (t0 + 14064U);
    t5 = *((char **)t1);
    t17 = *((int *)t5);
    t20 = (t17 - 0);
    t21 = (t20 * 1);
    t22 = (16U * t21);
    t23 = (0U + t22);
    t1 = (t0 + 16360);
    t6 = (t1 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t1, t23, 16U, 0LL);
    xsi_set_current_line(214, ng0);
    t1 = (t0 + 14064U);
    t2 = *((char **)t1);
    t17 = *((int *)t2);
    t3 = (t17 != 0);
    if (t3 != 0)
        goto LAB49;

LAB51:
LAB50:    goto LAB32;

LAB34:    t1 = (t0 + 6008U);
    t6 = *((char **)t1);
    t15 = *((unsigned char *)t6);
    t16 = (t15 == (unsigned char)2);
    t3 = t16;
    goto LAB36;

LAB37:    t1 = (t0 + 5528U);
    t5 = *((char **)t1);
    t13 = *((unsigned char *)t5);
    t14 = (t13 == (unsigned char)3);
    t4 = t14;
    goto LAB39;

LAB40:    xsi_set_current_line(199, ng0);
    t1 = (t0 + 16104);
    t8 = (t1 + 56U);
    t9 = *((char **)t8);
    t12 = (t9 + 56U);
    t19 = *((char **)t12);
    *((unsigned char *)t19) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(200, ng0);
    t1 = (t0 + 16168);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    goto LAB41;

LAB43:    xsi_set_current_line(207, ng0);
    t1 = (t0 + 14064U);
    t6 = *((char **)t1);
    t17 = *((int *)t6);
    t20 = (t17 - 1);
    t1 = (t0 + 14064U);
    t7 = *((char **)t1);
    t1 = (t7 + 0);
    *((int *)t1) = t20;
    goto LAB44;

LAB46:    t1 = (t0 + 6168U);
    t5 = *((char **)t1);
    t11 = *((unsigned char *)t5);
    t13 = (t11 == (unsigned char)2);
    t3 = t13;
    goto LAB48;

LAB49:    xsi_set_current_line(215, ng0);
    t1 = (t0 + 14064U);
    t5 = *((char **)t1);
    t20 = *((int *)t5);
    t24 = (t20 - 1);
    t1 = (t0 + 14064U);
    t6 = *((char **)t1);
    t1 = (t6 + 0);
    *((int *)t1) = t24;
    goto LAB50;

}


extern void work_a_3772128646_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3772128646_3212880686_p_0,(void *)work_a_3772128646_3212880686_p_1,(void *)work_a_3772128646_3212880686_p_2};
	xsi_register_didat("work_a_3772128646_3212880686", "isim/stack_tb_isim_beh.exe.sim/work/a_3772128646_3212880686.didat");
	xsi_register_executes(pe);
}
