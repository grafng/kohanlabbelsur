

--empty descending stack implementation in vhdl.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity stack is
	port
	(
		clk         : in std_logic; --clock for the stack.
		reset       : in std_logic; --active high reset.    
		enable      : in std_logic; --enable the stack. otherwise neither push nor pop will happen.
		data_in     : in std_logic_vector(9 downto 0); --data to be pushed to stack
		data_out    : out std_logic_vector(9 downto 0); --data popped from the stack.
		push_barpop : in std_logic; --active low for pop and active high for push.
		stack_full  : out std_logic; --goes high when the stack is full.
		stack_empty : out std_logic --goes high when the stack is empty.
	);
end stack;

architecture behavioral of stack is

	type mem_type is array (0 to 13) of std_logic_vector(9 downto 0);
	signal stack_mem   : mem_type  := (others => (others => '0'));
	signal full, empty : std_logic := '0';
	signal prev_pp     : std_logic := '0';
	signal sp          : integer   := 0; --for simulation and debugging. 

begin

	stack_full  <= full;
	stack_empty <= empty;

	--push and pop process for the stack.
	push : process (clk, reset)
		variable stack_ptr : integer := 13;
	begin
		if (reset = '1') then
			stack_ptr := 13; --stack grows downwards.
			full     <= '0';
			empty    <= '0';
			data_out <= (others => '0');
			prev_pp  <= '0';
		elsif (rising_edge(clk)) then
		
			--value of push_barpop with one clock cycle delay.
			if (enable = '1') then
				prev_pp <= push_barpop;
			else
				prev_pp <= '0';
			end if;
			
			--pop section.
			if (enable = '1' and push_barpop = '0' and empty = '0') then
				--setting empty flag.           
				if (stack_ptr = 13) then
					full  <= '0';
					empty <= '1';
				else
					full  <= '0';
					empty <= '0';
				end if;
				--when the push becomes pop, before stack is full. 
				if (prev_pp = '1' and full = '0') then
					stack_ptr := stack_ptr + 1;
				end if;
				--data has to be taken from the next highest address(empty descending type stack).              
				data_out <= stack_mem(stack_ptr);
				if (stack_ptr /= 13) then
					stack_ptr := stack_ptr + 1;
				end if;
			end if;

			--push section.
			if (enable = '1' and push_barpop = '1' and full = '0') then
				--setting full flag.
				if (stack_ptr = 0) then
					full  <= '1';
					empty <= '0';
				else
					full  <= '0';
					empty <= '0';
				end if;
				--when the pop becomes push, before stack is empty.
				if (prev_pp = '0' and empty = '0') then
					stack_ptr := stack_ptr - 1;
				end if;
				--data pushed to the current address.       
				stack_mem(stack_ptr) <= data_in;
				if (stack_ptr /= 0) then
					stack_ptr := stack_ptr - 1;
				end if;
			end if;
			
			sp <= stack_ptr; --for debugging/simulation.

		end if;
	end process;

end behavioral;
