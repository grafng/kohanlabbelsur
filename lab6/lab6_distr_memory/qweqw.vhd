library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity stack_tb is
end stack_tb;

architecture behavior of stack_tb is
	--Inputs and outputs
	signal Clk, reset, Enable, PUSH_barPOP, Stack_Full, Stack_Empty : std_logic := '0';
	signal Data_In, Data_Out : std_logic_vector(9 downto 0) := (others => '0');
	--temporary signals
	signal i : integer := 0;
	-- Clock period definitions
	constant Clk_period : time := 10 ns;

begin

	-- Instantiate the Unit Under Test (UUT)
	uut : entity work.stack port map
		(
		Clk         => Clk,
		reset       => reset,
		Enable      => Enable,
		Data_In     => Data_In,
		Data_Out    => Data_Out,
		PUSH_barPOP => PUSH_barPOP,
		Stack_Full  => Stack_Full,
		Stack_Empty => Stack_Empty
		);

	-- Clock process definitions
	Clk_process : process
	begin
		Clk <= '0';
		wait for Clk_period/2;
		Clk <= '1';
		wait for Clk_period/2;
	end process;

	-- Stimulus process
	stim_proc : process
	begin
	
		reset <= '1'; --apply reset for one clock cycle.
		wait for clk_period;
		reset <= '0';
		wait for clk_period * 3; --wait for 3 clock periods(simply)
		Enable <= '1'; --Enable stack
		wait for clk_period * 3; --wait for 3 clock periods(simply)
		PUSH_barPOP <= '1'; --Set for push operation.
		Data_In <= "1111111111"; --Push something to stack.
		wait for clk_period;
		Data_In <= "1100111111"; --Push something to stack.
		wait for clk_period;
		Data_In <= "1111111100"; --Push something to stack.
		wait for clk_period;
		PUSH_barPOP <= '0'; --POP two of the above pushed values.
		wait for clk_period * 2;
		PUSH_barPOP <= '1'; --Set for push operation.
		Data_In <= "1000000000"; --Push something to stack.
		wait for clk_period;
		Data_In <= "0000001111"; --Push something to stack.
		wait for clk_period;
		PUSH_barPOP <= '0'; --POP all the above pushed values.
		wait for clk_period * 10; --wait for some time.
		PUSH_barPOP <= '1'; --Set for push operation.
		Data_In <= "0000000011"; --Push something to stack.
		wait for clk_period;
		PUSH_barPOP <= '0'; --pop what was pushed.
		wait for clk_period * 10;
		Enable <= '0'; --disable stack.
		wait for clk_period * 10; --wait for some time.
		Enable      <= '1'; --Enable the stack.
		PUSH_barPOP <= '1'; --Set for push operation.
		
		for i in 0 to 15 loop --Push integers from 0 to 13 to the stack.
			Data_In <= conv_std_logic_vector(i, 10);
			wait for clk_period;
		end loop;
		Enable <= '0'; --disable the stack.
		wait for clk_period * 2;
		Enable      <= '1'; --re-enable the stack.
		PUSH_barPOP <= '0'; --Set for POP operation.
		
		for i in 0 to 15 loop --POP all elements from stack one by one.
			wait for clk_period;
		end loop;
		Enable <= '0'; --Disable stack.
		wait;
		
	end process;

end;