


LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY serial_tx_tb IS
END serial_tx_tb;
 
ARCHITECTURE behavior OF serial_tx_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT serial_tx
    PORT(
         in1 : IN  std_logic_vector(7 downto 0);
         out1 : OUT  std_logic;
         start : IN  std_logic;
         clk : IN  std_logic;
			CLR : inout std_logic;
			Q : inout std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal in1 : std_logic_vector(7 downto 0) := (others => '0');
   signal start : std_logic := '0';
   signal clk : std_logic := '0';
	signal CLR : std_logic;

 	--Outputs
   signal out1 : std_logic;
	signal COUNTER : std_logic_vector(3 downto 0);
  
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: serial_tx PORT MAP (
          in1 => in1,
          out1 => out1,
          start => start,
          clk => clk,
			 CLR => CLR,
			 Q => COUNTER
        );
	
	
	clk_process:
	process
	begin
		 -- ����������� ���� � ������� �������� �������������� �� ���� CLK
		 infinite_loop: loop
			  clk <= '1';
			  wait for 5ns;
			  clk <= '0';
			  wait for 5ns;
		 end loop infinite_loop;
	end process;
	

	data_process:
	process
	begin
			 start <= '0';
			 -- ������������� �������� �� ��������
			 in1 <= "01001101";
			 wait for 10ns;      
			 -- ��������� ��������
			 start <= '1';
			 wait for 10ns;
			 start <= '0';
			 wait for 5ns;
			 assert out1 = '1'
			 report "���� ���������� ����!"
			 severity failure;
			 wait for 10ns;
			 assert out1 = '1'
			 report "������ � 0-� ����!"
			 severity failure;
			 wait for 10ns;
			 assert out1 = '0'
			 report "������ � 1-� ����!"
			 severity failure;
			 wait for 10ns;
			 assert out1 = '1'
			 report "������ � 2-� ����!"
			 severity failure;
			 wait for 10ns;
			 assert out1 = '1'
			 report "������ � 3-� ����!"
			 severity failure;
			 wait for 10ns;
			 assert out1 = '0'
			 report "������ � 4-� ����!"
			 severity failure;
			 wait for 10ns;
			 assert out1 = '0'
			 report "������ � 5-� ����!"
			 severity failure;
			 wait for 10ns;
			 assert out1 = '1'
			 report "������ � 6-� ����!"
			 severity failure;
			 wait for 10ns;
			 assert out1 = '0'
			 report "������ � 7-� ����!"
			 severity failure;
			 wait for 10ns;
			 assert out1 = '1'
			 report "���� ��������� ����!"
			 severity failure;
		 report "����������! ���������� ����������������� ����� ������� ������ ������������!";
		 wait;
	end process;




END;






--
--   stim_proc: process
--   		begin
--				in1 <= X"1";
--				wait for 100ns;
--				in1 <= X"2";
--				wait for 100ns;
--				in1 <= X"3";
--				wait for 100ns;
--				in1 <= X"4";
--				wait for 100ns;
--				in1 <= X"5";
--				wait for 100ns;
--				in1 <= X"6";
--				wait for 100ns;
--				in1 <= X"7";
--				wait for 100ns;
--				in1 <= X"8";
--				wait for 100ns;
--				in1 <= X"9";
--				wait for 100ns;
--				in1 <= X"A";
--				wait for 100ns;
--				in1 <= X"B";
--				wait for 100ns;
--				in1 <= X"C";
--				wait;
--   end process;