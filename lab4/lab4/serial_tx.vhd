
------------------------------------------------
---------------MAIN----PROGRAM------------------
------------------------------------------------
-- implementation of the serial port transmitter
library ieee;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library UNISIM;
use UNISIM.VComponents.all;

entity serial_tx is
	port
	(
		in1   : in std_logic_vector(7 downto 0);
		in2   : inout std_logic;
		out1  : out std_logic;
		start : in std_logic;
		CLK   : in std_logic;
		CLR   : inout std_logic;
		Q     : inout std_logic_vector(3 downto 0)
	);
end serial_tx;

architecture Behavioral of serial_tx is


	--  SelectIO description(transmitter)
	attribute BOX_TYPE : string;
	component OBUF_LVCMOS33
		port
		(
			O : out std_logic;
			I : in std_logic
		);
	end component;
	attribute BOX_TYPE of
	OBUF_LVCMOS33  : component is "PRIMITIVE";

		signal COUNTER : std_logic_vector(3 downto 0) := "0000";
		
	begin

		--rerealization of transmitter		
		out_ibuf : OBUF_LVCMOS33 port map
		(
			i => in2,
			o => out1
		);	

		--binar counter	
		process (CLK)
		begin
			if (CLK'event and CLK = '1') then
				if (start = '1') then
					CLR <= '1';
				end if;
			else
				if (CLR = '1') then
					COUNTER <= COUNTER + '1';
				end if;
			end if;
		end process;

		--coder
		process (COUNTER)
		begin
			if (COUNTER = "0000") then
				in2 <= '0';
			end if;		
			if (COUNTER = "0001") then
				in2 <= '1';
			end if;
			if (COUNTER = "0010") then
				in2 <= in1(0);
			end if;
			if (COUNTER = "0011") then
				in2 <= in1(1);
			end if;
			if (COUNTER = "0100") then
				in2 <= in1(2);
			end if;
			if (COUNTER = "0101") then
				in2 <= in1(3);
			end if;
			if (COUNTER = "0110") then
				in2 <= in1(4);
			end if;
			if (COUNTER = "0111") then
				in2 <= in1(5);
			end if;
			if (COUNTER = "1000") then
				in2 <= in1(6);
			end if;
			if (COUNTER = "1001") then
				in2 <= in1(7);
			end if;
			if (COUNTER = "1010") then
				in2 <= '1';
			end if;
		end process;
		
		Q <= COUNTER;
		
	end Behavioral;

