
------------------------------------------------
---------------MAIN----PROGRAM------------------
------------------------------------------------
-- implementation of the serial port receiver
library ieee;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library UNISIM;
use UNISIM.VComponents.all;

entity serial_rx is
	port
	(
		in1   : in std_logic;
		out1  : out std_logic_vector (7 downto 0) := "00000000";
		out2  : inout std_logic;
		error : out std_logic  := '0';
		clk   : in std_logic;
		clr   : out std_logic;
		Q     : out std_logic_vector(3 downto 0) := "0000"
		
	);
end serial_rx;

architecture Behavioral of serial_rx is

	--  SelectIO description(receiver)
	attribute BOX_TYPE : string;
	component IBUF_LVCMOS33
		port
		(
			O : out std_logic;
			I : in std_logic
		);
	end component;
	attribute BOX_TYPE of
	IBUF_LVCMOS33  : component is "PRIMITIVE";

		signal COUNTER : std_logic_vector(3 downto 0) := "0000";	
		signal clrs     : std_logic := '0';

	begin

		--rerealization of receiver		
		qwertyuio : ibuf_lvcmos33 port map
			(
			i => in1, 
			o => out2
			);

		--binar counter
		process (CLK,clrs,counter,out2)
		begin
		
			if (out2 ='1') then
				clrs <= '1';
			end if;	
			
			if (COUNTER = "1101") then
				clrs <= '0';
				COUNTER <= "0000";
			end if;	
		
			if (CLK'event and (CLK = '0')) then
				if (clrs = '1') then
					COUNTER <= COUNTER + '1';
				end if;
			end if;
			
		end process;
		
		--decoder
		process (COUNTER)
		begin
			if (COUNTER = "0010") then
				out1(0) <= out2;
			end if;
			if (COUNTER = "0011") then
				out1(1) <= out2;
			end if;
			if (COUNTER = "0100") then
				out1(2) <= out2;
			end if;
			if (COUNTER = "0101") then
				out1(3) <= out2;
			end if;
			if (COUNTER = "0110") then
				out1(4) <= out2;
			end if;
			if (COUNTER = "0111") then
				out1(5) <= out2;
			end if;
			if (COUNTER = "1000") then
				out1(6) <= out2;
			end if;
			if (COUNTER = "1001") then
				out1(7) <= out2;
			end if;
			if (COUNTER = "1010") then
				if (in1 = '0') then
					error <= '1';
					error <= '0' after 50ns;
				end if;
			end if;
			if (COUNTER = "1011") then
				out1 <= "00000000";
			end if;

		end process;
		
		Q <= COUNTER;
		clr <= clrs;

end Behavioral;