/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "G:/projects/local vhdl/lab4/lab4/serial_tx_tb.vhd";



static void work_a_4118307140_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;

LAB0:    t1 = (t0 + 2992U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(58, ng0);

LAB4:
LAB5:    xsi_set_current_line(59, ng0);
    t2 = (t0 + 3624);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(60, ng0);
    t7 = (5 * 1000LL);
    t2 = (t0 + 2800);
    xsi_process_wait(t2, t7);

LAB10:    *((char **)t1) = &&LAB11;

LAB1:    return;
LAB6:;
LAB7:    goto LAB2;

LAB8:    xsi_set_current_line(61, ng0);
    t2 = (t0 + 3624);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(62, ng0);
    t7 = (5 * 1000LL);
    t2 = (t0 + 2800);
    xsi_process_wait(t2, t7);

LAB14:    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

LAB12:    goto LAB4;

LAB13:    goto LAB12;

LAB15:    goto LAB13;

}

static void work_a_4118307140_2372691052_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    int64 t9;
    unsigned char t10;
    unsigned char t11;

LAB0:    t1 = (t0 + 3240U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(70, ng0);
    t2 = (t0 + 3688);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(72, ng0);
    t2 = (t0 + 5924);
    t4 = (t0 + 3752);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t2, 8U);
    xsi_driver_first_trans_fast(t4);
    xsi_set_current_line(73, ng0);
    t9 = (10 * 1000LL);
    t2 = (t0 + 3048);
    xsi_process_wait(t2, t9);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(75, ng0);
    t2 = (t0 + 3688);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(76, ng0);
    t9 = (10 * 1000LL);
    t2 = (t0 + 3048);
    xsi_process_wait(t2, t9);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    xsi_set_current_line(77, ng0);
    t2 = (t0 + 3688);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(78, ng0);
    t9 = (5 * 1000LL);
    t2 = (t0 + 3048);
    xsi_process_wait(t2, t9);

LAB14:    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

LAB12:    xsi_set_current_line(79, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t10 = *((unsigned char *)t3);
    t11 = (t10 == (unsigned char)3);
    if (t11 == 0)
        goto LAB16;

LAB17:    xsi_set_current_line(82, ng0);
    t9 = (10 * 1000LL);
    t2 = (t0 + 3048);
    xsi_process_wait(t2, t9);

LAB20:    *((char **)t1) = &&LAB21;
    goto LAB1;

LAB13:    goto LAB12;

LAB15:    goto LAB13;

LAB16:    t2 = (t0 + 5932);
    xsi_report(t2, 21U, (unsigned char)3);
    goto LAB17;

LAB18:    xsi_set_current_line(83, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t10 = *((unsigned char *)t3);
    t11 = (t10 == (unsigned char)3);
    if (t11 == 0)
        goto LAB22;

LAB23:    xsi_set_current_line(86, ng0);
    t9 = (10 * 1000LL);
    t2 = (t0 + 3048);
    xsi_process_wait(t2, t9);

LAB26:    *((char **)t1) = &&LAB27;
    goto LAB1;

LAB19:    goto LAB18;

LAB21:    goto LAB19;

LAB22:    t2 = (t0 + 5953);
    xsi_report(t2, 18U, (unsigned char)3);
    goto LAB23;

LAB24:    xsi_set_current_line(87, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t10 = *((unsigned char *)t3);
    t11 = (t10 == (unsigned char)2);
    if (t11 == 0)
        goto LAB28;

LAB29:    xsi_set_current_line(90, ng0);
    t9 = (10 * 1000LL);
    t2 = (t0 + 3048);
    xsi_process_wait(t2, t9);

LAB32:    *((char **)t1) = &&LAB33;
    goto LAB1;

LAB25:    goto LAB24;

LAB27:    goto LAB25;

LAB28:    t2 = (t0 + 5971);
    xsi_report(t2, 18U, (unsigned char)3);
    goto LAB29;

LAB30:    xsi_set_current_line(91, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t10 = *((unsigned char *)t3);
    t11 = (t10 == (unsigned char)3);
    if (t11 == 0)
        goto LAB34;

LAB35:    xsi_set_current_line(94, ng0);
    t9 = (10 * 1000LL);
    t2 = (t0 + 3048);
    xsi_process_wait(t2, t9);

LAB38:    *((char **)t1) = &&LAB39;
    goto LAB1;

LAB31:    goto LAB30;

LAB33:    goto LAB31;

LAB34:    t2 = (t0 + 5989);
    xsi_report(t2, 18U, (unsigned char)3);
    goto LAB35;

LAB36:    xsi_set_current_line(95, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t10 = *((unsigned char *)t3);
    t11 = (t10 == (unsigned char)3);
    if (t11 == 0)
        goto LAB40;

LAB41:    xsi_set_current_line(98, ng0);
    t9 = (10 * 1000LL);
    t2 = (t0 + 3048);
    xsi_process_wait(t2, t9);

LAB44:    *((char **)t1) = &&LAB45;
    goto LAB1;

LAB37:    goto LAB36;

LAB39:    goto LAB37;

LAB40:    t2 = (t0 + 6007);
    xsi_report(t2, 18U, (unsigned char)3);
    goto LAB41;

LAB42:    xsi_set_current_line(99, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t10 = *((unsigned char *)t3);
    t11 = (t10 == (unsigned char)2);
    if (t11 == 0)
        goto LAB46;

LAB47:    xsi_set_current_line(102, ng0);
    t9 = (10 * 1000LL);
    t2 = (t0 + 3048);
    xsi_process_wait(t2, t9);

LAB50:    *((char **)t1) = &&LAB51;
    goto LAB1;

LAB43:    goto LAB42;

LAB45:    goto LAB43;

LAB46:    t2 = (t0 + 6025);
    xsi_report(t2, 18U, (unsigned char)3);
    goto LAB47;

LAB48:    xsi_set_current_line(103, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t10 = *((unsigned char *)t3);
    t11 = (t10 == (unsigned char)2);
    if (t11 == 0)
        goto LAB52;

LAB53:    xsi_set_current_line(106, ng0);
    t9 = (10 * 1000LL);
    t2 = (t0 + 3048);
    xsi_process_wait(t2, t9);

LAB56:    *((char **)t1) = &&LAB57;
    goto LAB1;

LAB49:    goto LAB48;

LAB51:    goto LAB49;

LAB52:    t2 = (t0 + 6043);
    xsi_report(t2, 18U, (unsigned char)3);
    goto LAB53;

LAB54:    xsi_set_current_line(107, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t10 = *((unsigned char *)t3);
    t11 = (t10 == (unsigned char)3);
    if (t11 == 0)
        goto LAB58;

LAB59:    xsi_set_current_line(110, ng0);
    t9 = (10 * 1000LL);
    t2 = (t0 + 3048);
    xsi_process_wait(t2, t9);

LAB62:    *((char **)t1) = &&LAB63;
    goto LAB1;

LAB55:    goto LAB54;

LAB57:    goto LAB55;

LAB58:    t2 = (t0 + 6061);
    xsi_report(t2, 18U, (unsigned char)3);
    goto LAB59;

LAB60:    xsi_set_current_line(111, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t10 = *((unsigned char *)t3);
    t11 = (t10 == (unsigned char)2);
    if (t11 == 0)
        goto LAB64;

LAB65:    xsi_set_current_line(114, ng0);
    t9 = (10 * 1000LL);
    t2 = (t0 + 3048);
    xsi_process_wait(t2, t9);

LAB68:    *((char **)t1) = &&LAB69;
    goto LAB1;

LAB61:    goto LAB60;

LAB63:    goto LAB61;

LAB64:    t2 = (t0 + 6079);
    xsi_report(t2, 18U, (unsigned char)3);
    goto LAB65;

LAB66:    xsi_set_current_line(115, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t10 = *((unsigned char *)t3);
    t11 = (t10 == (unsigned char)3);
    if (t11 == 0)
        goto LAB70;

LAB71:    xsi_set_current_line(118, ng0);
    t2 = (t0 + 6117);
    xsi_report(t2, 75U, 0);
    xsi_set_current_line(119, ng0);

LAB74:    *((char **)t1) = &&LAB75;
    goto LAB1;

LAB67:    goto LAB66;

LAB69:    goto LAB67;

LAB70:    t2 = (t0 + 6097);
    xsi_report(t2, 20U, (unsigned char)3);
    goto LAB71;

LAB72:    goto LAB2;

LAB73:    goto LAB72;

LAB75:    goto LAB73;

}


extern void work_a_4118307140_2372691052_init()
{
	static char *pe[] = {(void *)work_a_4118307140_2372691052_p_0,(void *)work_a_4118307140_2372691052_p_1};
	xsi_register_didat("work_a_4118307140_2372691052", "isim/serial_tx_tb_isim_beh.exe.sim/work/a_4118307140_2372691052.didat");
	xsi_register_executes(pe);
}
