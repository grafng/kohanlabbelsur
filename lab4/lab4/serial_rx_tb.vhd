--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:13:07 06/03/2016
-- Design Name:   
-- Module Name:   D:/GROMYKO_I_L/LAB_4/lab_4/serial_rx_tb.vhd
-- Project Name:  lab_4
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: serial_rx
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;


entity serial_rx_tb is
end serial_rx_tb;

architecture behavior of serial_rx_tb is

	-- Component Declaration for the Unit Under Test (UUT)

	component serial_rx
		port
		(
			in1   : in std_logic;
			out1  : out std_logic_vector(7 downto 0);
			out2  : inout std_logic;
			error : out std_logic;
			clk   : in std_logic;
			clr   : out std_logic;
	  	   Q     : out std_logic_vector(3 downto 0) := "0000"
		);
	end component;

	--Inputs
	signal in1   : std_logic;
	signal clk   : std_logic;
	signal clr   : std_logic;

	--Outputs
	signal out1  : std_logic_vector(7 downto 0);
	signal error : std_logic;
	signal out2  : std_logic;
	signal COUNTER : std_logic_vector(3 downto 0);

begin

	-- Instantiate the Unit Under Test (UUT)
	uut : serial_rx port map
	(
		in1   => in1,
		out1  => out1,
		out2  => out2,
		error => error,
		Q => COUNTER,
		-- counter => counter,
		clk   => clk,
		clr   => clr
	);
	clk_process :
	process
	begin
		-- ����������� ���� � ������� �������� �������������� �� ���� CLK
		infinite_loop : loop
			clk <= '1';
			wait for 5ns;
			clk <= '0';
			wait for 5ns;
		end loop infinite_loop;
	end process;


	data_process :
	process
	begin
		in1 <= '0';
		wait for 10ns;
		-- ��������� ���
		in1 <= '1';
		wait for 10 ns;
		-- ���� ������
		in1 <= '1';
		wait for 10 ns;
		in1 <= '0';
		wait for 10 ns;
		in1 <= '1';
		wait for 10 ns;
		in1 <= '1';
		wait for 10 ns;
		in1 <= '0';
		wait for 10 ns;
		in1 <= '0';
		wait for 10 ns;
		in1 <= '1';
		wait for 10 ns;
		in1 <= '0';
		wait for 10 ns;
		-- �������� ���
		in1 <= '1';
		wait for 10 ns;
		assert out1 = "01001101" and error = '0'
		report "������ ������ ������� ����������� �����"
			severity failure;
		in1 <= '0';
		wait for 50ns;
		-- ��������� ���
		in1 <= '1';
		wait for 10 ns;
		-- ���� ������
		in1 <= '1';
		wait for 10 ns;
		in1 <= '0';
		wait for 10 ns;
		in1 <= '1';
		wait for 10 ns;
		in1 <= '1';
		wait for 10 ns;
		in1 <= '0';
		wait for 10 ns;
		in1 <= '0';
		wait for 10 ns;
		in1 <= '1';
		wait for 10 ns;
		in1 <= '0';
		wait for 10 ns;
		-- ��������� �������� ���
		in1 <= '0';
		wait for 10 ns;
		assert error = '1'
		report "�� ���������� ������ �� ������ �����"
			severity failure;
		in1 <= '0';
		wait for 50ns;
		-- ��������� ���
		in1 <= '1';
		wait for 10 ns;
		-- ���� ������
		in1 <= '0';
		wait for 10 ns;
		in1 <= '1';
		wait for 10 ns;
		in1 <= '0';
		wait for 10 ns;
		in1 <= '0';
		wait for 10 ns;
		in1 <= '1';
		wait for 10 ns;
		in1 <= '1';
		wait for 10 ns;
		in1 <= '0';
		wait for 10 ns;
		in1 <= '1';
		wait for 10 ns;
		-- c������� ���
		in1 <= '1';
		wait for 10 ns;
		assert out1 = "10110010" and error = '0'
		report "������ ������ �������� ����������� �����"
			severity failure;
		report "����������! ������� ����������������� ����� ������� ������ ������������!";
		wait;
	end process;

end;