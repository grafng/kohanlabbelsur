
------------------------------------------------
---------------MAIN----PROGRAM------------------
------------------------------------------------
--------- implementation binary coded counter---
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

--�������� ������:
--CLK - �������� �������;
--RESET - ����������� �����;
--ENABLE - ���� ����������;
--FULL - ���� � ������������ ��������;
--BCD_U - ����� ������;	
--BCD_D - ����� ��������;

entity binary_coded_counter is
	port
	(
		CLK    : in STD_LOGIC;
		RESET  : in STD_LOGIC;
		ENABLE : in STD_LOGIC;
		FULL   : out STD_LOGIC;
		BCD_U  : out STD_LOGIC_VECTOR (3 downto 0);
		BCD_D  : out STD_LOGIC_VECTOR (3 downto 0);
		clk_out  : inout std_logic := '0'
	);
end binary_coded_counter;



architecture behavior of binary_coded_counter is

	signal COUNTER_U : integer range 0 to 9;
	signal COUNTER_D : integer range 0 to 9;
	signal COUNTER_H : integer range 0 to 9;
	signal COUNTER_T : integer range 0 to 9;
	signal IS_9999   : STD_LOGIC;

	signal divider   : std_logic_vector(4 downto 0) := "00000";

begin



	--implementation divider
	process (clk,reset)
	begin	
		if reset = '1' then
			divider <= "00000";
		end if;
		if clk'event and clk = '1' then
			divider <= divider + 1;
		end if;
	end process;
	
	clk_out <= '1' when conv_integer(divider) = 17 else '0';

	--implementation binary coded counter
	process (clk_out, RESET)
	begin
		if RESET = '1' then
			COUNTER_U <= 0;
			COUNTER_D <= 0;
			COUNTER_H <= 0;
			COUNTER_T <= 0;
		elsif clk_out = '1' and clk_out'event then
			if ENABLE = '1' and IS_9999 = '0' then
				if COUNTER_U = 0 then
					COUNTER_U <= 9;
					if COUNTER_D = 0 then
						COUNTER_D <= 9;
						if COUNTER_H = 0 then
							COUNTER_H <= 9;
							COUNTER_T <= COUNTER_T - 1;
						else
							COUNTER_H <= COUNTER_H - 1;
						end if;
					else
						COUNTER_D <= COUNTER_D - 1;
					end if;
				else
					COUNTER_U <= COUNTER_U - 1;
				end if;
			end if;
		end if;
	end process;

	BCD_U   <= CONV_STD_LOGIC_VECTOR(COUNTER_U, 4);
	BCD_D   <= CONV_STD_LOGIC_VECTOR(COUNTER_D, 4);
	IS_9999 <= '1' when (COUNTER_U = 9 and COUNTER_D = 9 and COUNTER_H = 9 and COUNTER_T = 9) else '0';
	FULL    <= IS_9999;
	
end behavior;