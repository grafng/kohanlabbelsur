
------------------------------------------------
---------------test_bench-----------------------
------------------------------------------------
--------- implementation binary coded counter---

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity binary_coded_counter_tb is
end binary_coded_counter_tb;

architecture behavior of binary_coded_counter_tb is

	component binary_coded_counter
		port
		(
		
			CLK: in STD_LOGIC;
			RESET: in STD_LOGIC;
			ENABLE: in STD_LOGIC;
			FULL: out STD_LOGIC;
			BCD_U: out STD_LOGIC_VECTOR (3 downto 0);    
			BCD_D: out STD_LOGIC_VECTOR (3 downto 0);
			clk_out   : inout std_logic	
		);	
	end component;

	--Inputs
	signal clk    : std_logic;
	signal RESET  : STD_LOGIC := '0';
	signal ENABLE : STD_LOGIC := '1';
	signal BCD_U     : std_logic_vector(3 downto 0);
	signal BCD_D     : std_logic_vector(3 downto 0);
	
	--Outputs
	signal clk_out   : std_logic;
	
begin

	-- Instantiate the Unit Under Test (UUT)
	uut : binary_coded_counter port map
	(
		RESET => RESET,
		ENABLE => ENABLE,
		clk   => clk,
		BCD_U => BCD_U,
		BCD_D => BCD_D,
		clk_out => clk_out
	);
	
	clk_process :
	process
	begin
		--an infinite loop in which clock pulses are output to the input CLK
		infinite_loop : loop
			clk <= '1';
			wait for 5ns;
			clk <= '0';
			wait for 5ns;
		end loop infinite_loop;
	end process;
	
end;