--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : shem.vhf
-- /___/   /\     Timestamp : 06/21/2018 07:26:15
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan3e -flat -suppress -vhdl "C:/Users/qwerty/Desktop/local vhdl/lab1/shemnew/shem.vhf" -w "C:/Users/qwerty/Desktop/local vhdl/lab1/shemnew/shem.sch"
--Design Name: shem
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity shem is
   port ( a       : in    std_logic; 
          b       : in    std_logic; 
          c       : in    std_logic; 
          d       : in    std_logic; 
          f_out   : out   std_logic; 
          XLXN_14 : out   std_logic; 
          XLXN_15 : out   std_logic; 
          XLXN_16 : out   std_logic; 
          XLXN_19 : out   std_logic);
end shem;

architecture BEHAVIORAL of shem is
   attribute BOX_TYPE   : string ;
   signal XLXN_22       : std_logic;
   signal XLXN_14_DUMMY : std_logic;
   signal XLXN_15_DUMMY : std_logic;
   signal XLXN_16_DUMMY : std_logic;
   signal XLXN_19_DUMMY : std_logic;
   component AND3B3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3B3 : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component AND3B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3B1 : component is "BLACK_BOX";
   
   component OR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR4 : component is "BLACK_BOX";
   
begin
   XLXN_14 <= XLXN_14_DUMMY;
   XLXN_15 <= XLXN_15_DUMMY;
   XLXN_16 <= XLXN_16_DUMMY;
   XLXN_19 <= XLXN_19_DUMMY;
   XLXI_9 : AND3B3
      port map (I0=>c,
                I1=>b,
                I2=>a,
                O=>XLXN_19_DUMMY);
   
   XLXI_10 : AND3
      port map (I0=>d,
                I1=>b,
                I2=>a,
                O=>XLXN_14_DUMMY);
   
   XLXI_12 : AND3B1
      port map (I0=>a,
                I1=>d,
                I2=>c,
                O=>XLXN_15_DUMMY);
   
   XLXI_15 : AND3B1
      port map (I0=>d,
                I1=>c,
                I2=>b,
                O=>XLXN_16_DUMMY);
   
   XLXI_18 : OR4
      port map (I0=>XLXN_16_DUMMY,
                I1=>XLXN_15_DUMMY,
                I2=>XLXN_14_DUMMY,
                I3=>XLXN_19_DUMMY,
                O=>f_out);
   
end BEHAVIORAL;


