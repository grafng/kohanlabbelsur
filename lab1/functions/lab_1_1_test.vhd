--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:33:22 05/25/2016
-- Design Name:   
-- Module Name:   E:/Gromyko_I_L/lab_1/lab_1_1_test.vhd
-- Project Name:  lab_1
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: lab_1_1
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
ENTITY lab_1_1_test IS
END lab_1_1_test;
 
ARCHITECTURE behavior OF lab_1_1_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT lab_1_1
    PORT(
         X : IN  std_logic_vector(8 downto 1);
         Y : OUT  std_logic_vector(5 downto 1)
        );
    END COMPONENT;
    
	
   --Inputs
   signal X : std_logic_vector(8 downto 1);

 	--Outputs
   signal Y : std_logic_vector(5 downto 1);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: lab_1_1 PORT MAP (
          X => X,
          Y => Y
        );

   -- Stimulus process

		stim_proc: process
   		begin
			X <= (others => '0');
			for I in 0 to 255 loop
				wait for 100ns;
				X<=X+1;
				end loop;
				wait;
    	end process;
END;
