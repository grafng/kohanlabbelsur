





library IEEE;
use IEEE.STD_LOGIC_1164.ALL;





entity lab_1_1 is
    Port ( X : in  STD_LOGIC_VECTOR (8 downto 1);
           Y : out  STD_LOGIC_VECTOR (5 downto 1));
end lab_1_1;

architecture Behavioral of lab_1_1 is

begin


	Y(1) <= (X(5) and X(4) and (not (X(5) or (not X(3))))) xor X(6);
	Y(2) <= X(7) or (X(8) and not (X(7) and not (X(4) and not X(4))));
	Y(3) <= (X(3) or X(1)) xor (X(7) or ((not X(1)) and (not X(8))));
	Y(4) <= (X(4) and not ((X(2) and not (X(6) or X(3))) or X(8))) or X(2);
	Y(5) <= X(6) or (X(7) and (X(8) xor (X(2) xor X(6))) and (not X(8))) or X(5) or X(5);
	
	
end Behavioral;

