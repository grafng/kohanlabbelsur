/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "E:/Gromyko_I_L/lab_1/lab_1_1.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1605435078_503743352(char *, unsigned char , unsigned char );
unsigned char ieee_p_2592010699_sub_1690584930_503743352(char *, unsigned char );
unsigned char ieee_p_2592010699_sub_2507238156_503743352(char *, unsigned char , unsigned char );
unsigned char ieee_p_2592010699_sub_2545490612_503743352(char *, unsigned char , unsigned char );


static void work_a_0139741422_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    unsigned int t4;
    unsigned int t5;
    unsigned int t6;
    unsigned char t7;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned char t14;
    unsigned char t15;
    unsigned char t16;
    char *t17;
    char *t18;
    int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned char t23;
    unsigned char t24;
    char *t25;
    char *t26;
    int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    unsigned char t31;
    unsigned char t32;
    unsigned char t33;
    unsigned char t34;
    char *t35;
    char *t36;
    char *t37;
    char *t38;
    char *t39;
    char *t40;

LAB0:    xsi_set_current_line(41, ng0);

LAB3:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t3 = (6 - 8);
    t4 = (t3 * -1);
    t5 = (1U * t4);
    t6 = (0 + t5);
    t1 = (t2 + t6);
    t7 = *((unsigned char *)t1);
    t8 = (t0 + 1032U);
    t9 = *((char **)t8);
    t10 = (3 - 8);
    t11 = (t10 * -1);
    t12 = (1U * t11);
    t13 = (0 + t12);
    t8 = (t9 + t13);
    t14 = *((unsigned char *)t8);
    t15 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t14);
    t16 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t7, t15);
    t17 = (t0 + 1032U);
    t18 = *((char **)t17);
    t19 = (8 - 8);
    t20 = (t19 * -1);
    t21 = (1U * t20);
    t22 = (0 + t21);
    t17 = (t18 + t22);
    t23 = *((unsigned char *)t17);
    t24 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t23);
    t25 = (t0 + 1032U);
    t26 = *((char **)t25);
    t27 = (2 - 8);
    t28 = (t27 * -1);
    t29 = (1U * t28);
    t30 = (0 + t29);
    t25 = (t26 + t30);
    t31 = *((unsigned char *)t25);
    t32 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t31);
    t33 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t24, t32);
    t34 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t16, t33);
    t35 = (t0 + 3808);
    t36 = (t35 + 56U);
    t37 = *((char **)t36);
    t38 = (t37 + 56U);
    t39 = *((char **)t38);
    *((unsigned char *)t39) = t34;
    xsi_driver_first_trans_delta(t35, 4U, 1, 0LL);

LAB2:    t40 = (t0 + 3664);
    *((int *)t40) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0139741422_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    unsigned int t4;
    unsigned int t5;
    unsigned int t6;
    unsigned char t7;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned char t14;
    char *t15;
    char *t16;
    int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned char t21;
    char *t22;
    char *t23;
    int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned char t28;
    unsigned char t29;
    unsigned char t30;
    unsigned char t31;
    unsigned char t32;
    char *t33;
    char *t34;
    int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned char t39;
    unsigned char t40;
    unsigned char t41;
    unsigned char t42;
    char *t43;
    char *t44;
    int t45;
    unsigned int t46;
    unsigned int t47;
    unsigned int t48;
    unsigned char t49;
    unsigned char t50;
    char *t51;
    char *t52;
    char *t53;
    char *t54;
    char *t55;
    char *t56;

LAB0:    xsi_set_current_line(42, ng0);

LAB3:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t3 = (3 - 8);
    t4 = (t3 * -1);
    t5 = (1U * t4);
    t6 = (0 + t5);
    t1 = (t2 + t6);
    t7 = *((unsigned char *)t1);
    t8 = (t0 + 1032U);
    t9 = *((char **)t8);
    t10 = (3 - 8);
    t11 = (t10 * -1);
    t12 = (1U * t11);
    t13 = (0 + t12);
    t8 = (t9 + t13);
    t14 = *((unsigned char *)t8);
    t15 = (t0 + 1032U);
    t16 = *((char **)t15);
    t17 = (2 - 8);
    t18 = (t17 * -1);
    t19 = (1U * t18);
    t20 = (0 + t19);
    t15 = (t16 + t20);
    t21 = *((unsigned char *)t15);
    t22 = (t0 + 1032U);
    t23 = *((char **)t22);
    t24 = (3 - 8);
    t25 = (t24 * -1);
    t26 = (1U * t25);
    t27 = (0 + t26);
    t22 = (t23 + t27);
    t28 = *((unsigned char *)t22);
    t29 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t28);
    t30 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t21, t29);
    t31 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t30);
    t32 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t14, t31);
    t33 = (t0 + 1032U);
    t34 = *((char **)t33);
    t35 = (6 - 8);
    t36 = (t35 * -1);
    t37 = (1U * t36);
    t38 = (0 + t37);
    t33 = (t34 + t38);
    t39 = *((unsigned char *)t33);
    t40 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t32, t39);
    t41 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t40);
    t42 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t7, t41);
    t43 = (t0 + 1032U);
    t44 = *((char **)t43);
    t45 = (5 - 8);
    t46 = (t45 * -1);
    t47 = (1U * t46);
    t48 = (0 + t47);
    t43 = (t44 + t48);
    t49 = *((unsigned char *)t43);
    t50 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t42, t49);
    t51 = (t0 + 3872);
    t52 = (t51 + 56U);
    t53 = *((char **)t52);
    t54 = (t53 + 56U);
    t55 = *((char **)t54);
    *((unsigned char *)t55) = t50;
    xsi_driver_first_trans_delta(t51, 3U, 1, 0LL);

LAB2:    t56 = (t0 + 3680);
    *((int *)t56) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0139741422_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    unsigned int t4;
    unsigned int t5;
    unsigned int t6;
    unsigned char t7;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned char t14;
    char *t15;
    char *t16;
    int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned char t21;
    char *t22;
    char *t23;
    int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned char t28;
    unsigned char t29;
    unsigned char t30;
    unsigned char t31;
    unsigned char t32;
    unsigned char t33;
    unsigned char t34;
    char *t35;
    char *t36;
    char *t37;
    char *t38;
    char *t39;
    char *t40;

LAB0:    xsi_set_current_line(43, ng0);

LAB3:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t3 = (5 - 8);
    t4 = (t3 * -1);
    t5 = (1U * t4);
    t6 = (0 + t5);
    t1 = (t2 + t6);
    t7 = *((unsigned char *)t1);
    t8 = (t0 + 1032U);
    t9 = *((char **)t8);
    t10 = (1 - 8);
    t11 = (t10 * -1);
    t12 = (1U * t11);
    t13 = (0 + t12);
    t8 = (t9 + t13);
    t14 = *((unsigned char *)t8);
    t15 = (t0 + 1032U);
    t16 = *((char **)t15);
    t17 = (3 - 8);
    t18 = (t17 * -1);
    t19 = (1U * t18);
    t20 = (0 + t19);
    t15 = (t16 + t20);
    t21 = *((unsigned char *)t15);
    t22 = (t0 + 1032U);
    t23 = *((char **)t22);
    t24 = (7 - 8);
    t25 = (t24 * -1);
    t26 = (1U * t25);
    t27 = (0 + t26);
    t22 = (t23 + t27);
    t28 = *((unsigned char *)t22);
    t29 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t28);
    t30 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t21, t29);
    t31 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t30);
    t32 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t14, t31);
    t33 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t32);
    t34 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t7, t33);
    t35 = (t0 + 3936);
    t36 = (t35 + 56U);
    t37 = *((char **)t36);
    t38 = (t37 + 56U);
    t39 = *((char **)t38);
    *((unsigned char *)t39) = t34;
    xsi_driver_first_trans_delta(t35, 2U, 1, 0LL);

LAB2:    t40 = (t0 + 3696);
    *((int *)t40) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0139741422_3212880686_p_3(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    unsigned int t4;
    unsigned int t5;
    unsigned int t6;
    unsigned char t7;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned char t14;
    char *t15;
    char *t16;
    int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned char t21;
    char *t22;
    char *t23;
    int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned char t28;
    unsigned char t29;
    unsigned char t30;
    unsigned char t31;
    unsigned char t32;
    char *t33;
    char *t34;
    int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned char t39;
    unsigned char t40;
    unsigned char t41;
    unsigned char t42;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;

LAB0:    xsi_set_current_line(44, ng0);

LAB3:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t3 = (7 - 8);
    t4 = (t3 * -1);
    t5 = (1U * t4);
    t6 = (0 + t5);
    t1 = (t2 + t6);
    t7 = *((unsigned char *)t1);
    t8 = (t0 + 1032U);
    t9 = *((char **)t8);
    t10 = (5 - 8);
    t11 = (t10 * -1);
    t12 = (1U * t11);
    t13 = (0 + t12);
    t8 = (t9 + t13);
    t14 = *((unsigned char *)t8);
    t15 = (t0 + 1032U);
    t16 = *((char **)t15);
    t17 = (6 - 8);
    t18 = (t17 * -1);
    t19 = (1U * t18);
    t20 = (0 + t19);
    t15 = (t16 + t20);
    t21 = *((unsigned char *)t15);
    t22 = (t0 + 1032U);
    t23 = *((char **)t22);
    t24 = (5 - 8);
    t25 = (t24 * -1);
    t26 = (1U * t25);
    t27 = (0 + t26);
    t22 = (t23 + t27);
    t28 = *((unsigned char *)t22);
    t29 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t28);
    t30 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t21, t29);
    t31 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t14, t30);
    t32 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t31);
    t33 = (t0 + 1032U);
    t34 = *((char **)t33);
    t35 = (2 - 8);
    t36 = (t35 * -1);
    t37 = (1U * t36);
    t38 = (0 + t37);
    t33 = (t34 + t38);
    t39 = *((unsigned char *)t33);
    t40 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t39);
    t41 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t32, t40);
    t42 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t7, t41);
    t43 = (t0 + 4000);
    t44 = (t43 + 56U);
    t45 = *((char **)t44);
    t46 = (t45 + 56U);
    t47 = *((char **)t46);
    *((unsigned char *)t47) = t42;
    xsi_driver_first_trans_delta(t43, 1U, 1, 0LL);

LAB2:    t48 = (t0 + 3712);
    *((int *)t48) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0139741422_3212880686_p_4(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    unsigned int t4;
    unsigned int t5;
    unsigned int t6;
    unsigned char t7;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned char t14;
    char *t15;
    char *t16;
    int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned char t21;
    unsigned char t22;
    char *t23;
    char *t24;
    int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned char t29;
    unsigned char t30;
    char *t31;
    char *t32;
    int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned char t37;
    unsigned char t38;
    unsigned char t39;
    unsigned char t40;
    unsigned char t41;
    char *t42;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;

LAB0:    xsi_set_current_line(45, ng0);

LAB3:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t3 = (3 - 8);
    t4 = (t3 * -1);
    t5 = (1U * t4);
    t6 = (0 + t5);
    t1 = (t2 + t6);
    t7 = *((unsigned char *)t1);
    t8 = (t0 + 1032U);
    t9 = *((char **)t8);
    t10 = (1 - 8);
    t11 = (t10 * -1);
    t12 = (1U * t11);
    t13 = (0 + t12);
    t8 = (t9 + t13);
    t14 = *((unsigned char *)t8);
    t15 = (t0 + 1032U);
    t16 = *((char **)t15);
    t17 = (4 - 8);
    t18 = (t17 * -1);
    t19 = (1U * t18);
    t20 = (0 + t19);
    t15 = (t16 + t20);
    t21 = *((unsigned char *)t15);
    t22 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t14, t21);
    t23 = (t0 + 1032U);
    t24 = *((char **)t23);
    t25 = (3 - 8);
    t26 = (t25 * -1);
    t27 = (1U * t26);
    t28 = (0 + t27);
    t23 = (t24 + t28);
    t29 = *((unsigned char *)t23);
    t30 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t29);
    t31 = (t0 + 1032U);
    t32 = *((char **)t31);
    t33 = (1 - 8);
    t34 = (t33 * -1);
    t35 = (1U * t34);
    t36 = (0 + t35);
    t31 = (t32 + t36);
    t37 = *((unsigned char *)t31);
    t38 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t37);
    t39 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t30, t38);
    t40 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t22, t39);
    t41 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t7, t40);
    t42 = (t0 + 4064);
    t43 = (t42 + 56U);
    t44 = *((char **)t43);
    t45 = (t44 + 56U);
    t46 = *((char **)t45);
    *((unsigned char *)t46) = t41;
    xsi_driver_first_trans_delta(t42, 0U, 1, 0LL);

LAB2:    t47 = (t0 + 3728);
    *((int *)t47) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_0139741422_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0139741422_3212880686_p_0,(void *)work_a_0139741422_3212880686_p_1,(void *)work_a_0139741422_3212880686_p_2,(void *)work_a_0139741422_3212880686_p_3,(void *)work_a_0139741422_3212880686_p_4};
	xsi_register_didat("work_a_0139741422_3212880686", "isim/lab_1_1_test_isim_beh.exe.sim/work/a_0139741422_3212880686.didat");
	xsi_register_executes(pe);
}
